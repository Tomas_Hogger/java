import javax.swing.*;
import java.awt.*;

public class Loader {
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        Form form = new Form();
        jFrame.setContentPane(form.getRootPanel());


        jFrame.setSize(800,600);
        jFrame.setMinimumSize(new Dimension(800,600));
        jFrame.setLocationRelativeTo(null);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setVisible(true);
    }
}
