import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyFrame extends JFrame {


    private Form1 form1 = new Form1();
    private Form2 form2 = new Form2();

    private final int PERSON_OK = 0, SURNAME_MISSING = 1, NAME_MISSING = 2, OTCHESTVO_MISSING = 3, INCORRECT_INPUT = 4;


    public MyFrame() {
        setContentPane(form1.getRootPanel());
        setSize(800, 600);
        setMinimumSize(new Dimension(800, 600));
        setTitle("ФИО");
        setDefaultCloseOperation(EXIT_ON_CLOSE);


        //============
        form1.getButton1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setPane();
            }
        });

        form2.getButton1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setPane();
            }
        });


        //==============


        form1.getButton1().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("control ENTER"), "pressed");
        form1.getButton1().getActionMap().put("pressed", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setPane();
            }
        });

        form2.getButton1().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("control ENTER"), "pressed");
        form2.getButton1().getActionMap().put("pressed", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setPane();
            }
        });

    }

    private void setPane(){
        if(getContentPane() == form1.getRootPanel()){
            setPane1();
        } else if(getContentPane() == form2.getRootPanel()){
            setPane2();
        } else {
            throw new IllegalStateException();
        }
    }

    private void setPane2(){
        Person person = form2.getPerson();
        if(canSwitch(checkFIO(person))) {
            form1.setPerson(person);
            setPanel(form1.getRootPanel());
        }
    }

    private void setPane1(){
        Person person = form1.getPerson();
        if(canSwitch(checkFIO(person))) {
            form2.setPerson(person);
            setPanel(form2.getRootPanel());
        }
    }

    private void setPanel(JPanel panel){
        setContentPane(panel);
        revalidate();
        repaint();
    }

    private boolean isEmpty(String txt){
        return txt == null || txt.trim().isEmpty();
    }

    private int checkFIO(Person person){
        if(person == null){
            return INCORRECT_INPUT;
        } else if(isEmpty(person.getSurname())){
            return SURNAME_MISSING;
        } else if (isEmpty(person.getName())){
            return NAME_MISSING;
        } else if (isEmpty(person.getOtchestvo())){
            return OTCHESTVO_MISSING;
        } else {
            return PERSON_OK;
        }
    }


    private boolean canSwitch(int check){
        if(check == PERSON_OK) {
            return true;
        } else if(check == SURNAME_MISSING) {
            JOptionPane.showMessageDialog(form1.getRootPanel(), "Введите фамилию", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } else if(check == NAME_MISSING){
            JOptionPane.showMessageDialog(form1.getRootPanel(),"Введите имя!","Error",JOptionPane.ERROR_MESSAGE);
            return false;
        } else if(check == OTCHESTVO_MISSING) {
            if(JOptionPane.showConfirmDialog(form1.getRootPanel(),"Вы точно хотите оставить поле без отчества?","Warning",JOptionPane.YES_NO_OPTION) == 0){
                return  true;
            } else {
                form1.getOtchestvo().requestFocusInWindow();
                return false;
            }
        } else if(check == INCORRECT_INPUT){
            JOptionPane.showMessageDialog(form1.getRootPanel(), "Некоректный ввод данных!", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } else {
            throw new IllegalStateException();
        }
    }




    public static void main(String[] args) {
        MyFrame myFrame = new MyFrame();
        myFrame.setLocationRelativeTo(null);
        myFrame.setVisible(true);
    }

    }


