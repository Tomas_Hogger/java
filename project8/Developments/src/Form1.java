import javax.swing.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Form1{
    private JPanel rootPanel;
    private JTextField surname;
    private JTextField name;
    private JTextField otchestvo;
    private JButton button1;

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public JButton getButton1() {
        return button1;
    }

    public JTextField getName(){
        return name;
    }

    public JTextField getSurname(){
        return surname;
    }

    public JTextField getOtchestvo(){
        return otchestvo;
    }



    public void setPerson(Person person){
        setTextFIO(person.getSurname(), person.getName(),  person.getOtchestvo());
    }

    private void setTextFIO(String surname, String name, String otchestvo){
        this.name.setText(name);
        this.surname.setText(surname);
        this.otchestvo.setText(otchestvo);
    }

    public Person getPerson(){
        return new Person(surname.getText().trim(),name.getText().trim(),otchestvo.getText().trim());
    }

}
