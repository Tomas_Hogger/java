import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Form2{
    private JPanel rootPanel;
    private JTextField fio;
    private JButton button1;
    private JProgressBar progressBar1;

    public Form2(){
        fio.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                progressBar1.setValue(fio.getText().trim().length());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                progressBar1.setValue(fio.getText().trim().length());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                progressBar1.setValue(fio.getText().trim().length());
            }
        });
    }


    public JPanel getRootPanel() {
        return rootPanel;
    }

    public JButton getButton1() {
        return button1;
    }






    public void setPerson(Person person){
        setTextFio(person.getSurname(), person.getName(),  person.getOtchestvo());
    }

    private void setTextFio(String surname, String name, String otchestvo){
        fio.setText(surname + " " + name + " " + otchestvo);
    }


    public Person getPerson(){
        String[] person = fio.getText().trim().split("\\s+");
        switch (person.length){
            case 0:
                return new Person();
            case 1:
                return new Person(person[0]," ", " ");
            case 2:
                return new Person(person[0], person[1], " ");
            case 3:
                return new Person(person[0], person[1], person[2]);
            default:
                return null;

        }
    }
}
