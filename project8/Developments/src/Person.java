public class Person {
    private String name;
    private String surname;
    private String otchestvo;


    public Person(){
        this("", "","");
    }

    public Person(String surname, String name, String otchestvo){
        this.surname = surname;
        this.name = name;
        this.otchestvo = otchestvo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }
}
