package figure;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Cone {

    static Color color;

    static List<Triangle> tris = new ArrayList<>();

    public static List<Triangle> getFigure(){

        color = Color.ORANGE;

        tris.add(new Triangle(new Vertex(-10, 100, -100, 1),
                new Vertex(-10, -100, -100, 1),
                new Vertex(0, -100, -100, 1),
                color));

        tris.add(new Triangle(new Vertex(-10, 100, -100, 1),
                new Vertex(0, 100, -100, 1),
                new Vertex(0, -100, -100, 1),
                color));






        tris.add(new Triangle(new Vertex(0, 100, -100, 1),
                new Vertex(0, -100, -100, 1),
                new Vertex(10, -100, -90, 1),
                color));

        tris.add(new Triangle(new Vertex(0, 100, -100, 1),
                new Vertex(10, 100, -90, 1),
                new Vertex(10, -100, -90, 1),
                color));

        tris.add(new Triangle(new Vertex(10, 100, -90, 1),
                new Vertex(10, -100, -90, 1),
                new Vertex(20, -100, -80, 1),
                color));

        tris.add(new Triangle(new Vertex(10, 100, -90, 1),
                new Vertex(20, 100, -80, 1),
                new Vertex(20, -100, -80, 1),
                color));






        tris.add(new Triangle(new Vertex(20, 100, -80, 1),
                new Vertex(20, -100, -80, 1),
                new Vertex(20, -100, -70, 1),
                color));

        tris.add(new Triangle(new Vertex(20, 100, -80, 1),
                new Vertex(20, 100, -70, 1),
                new Vertex(20, -100, -70, 1),
                color));





        tris.add(new Triangle(new Vertex(20, 100, -70, 1),
                new Vertex(20, -100, -70, 1),
                new Vertex(10, -100, -60, 1),
                color));

        tris.add(new Triangle(new Vertex(20, 100, -70, 1),
                new Vertex(10, 100, -60, 1),
                new Vertex(10, -100, -60, 1),
                color));

        tris.add(new Triangle(new Vertex(10, 100, -60, 1),
                new Vertex(10, -100, -60, 1),
                new Vertex(0, -100, -50, 1),
                color));

        tris.add(new Triangle(new Vertex(10, 100, -60, 1),
                new Vertex(0, 100, -50, 1),
                new Vertex(0, -100, -50, 1),
                color));





        tris.add(new Triangle(new Vertex(0, 100, -50, 1),
                new Vertex(0, -100, -50, 1),
                new Vertex(-10, -100, -50, 1),
                color));

        tris.add(new Triangle(new Vertex(0, 100, -50, 1),
                new Vertex(-10, 100, -50, 1),
                new Vertex(-10, -100, -50, 1),
                color));





        tris.add(new Triangle(new Vertex(-10, 100, -50, 1),
                new Vertex(-10, -100, -50, 1),
                new Vertex(-20, -100, -60, 1),
                color));

        tris.add(new Triangle(new Vertex(-10, 100, -50, 1),
                new Vertex(-20, 100, -60, 1),
                new Vertex(-20, -100, -60, 1),
                color));


        tris.add(new Triangle(new Vertex(-20, 100, -60, 1),
                new Vertex(-20, -100, -60, 1),
                new Vertex(-30, -100, -70, 1),
                color));

        tris.add(new Triangle(new Vertex(-20, 100, -60, 1),
                new Vertex(-30, 100, -70, 1),
                new Vertex(-30, -100, -70, 1),
                color));





        tris.add(new Triangle(new Vertex(-30, 100, -70, 1),
                new Vertex(-30, -100, -70, 1),
                new Vertex(-30, -100, -80, 1),
                color));

        tris.add(new Triangle(new Vertex(-30, 100, -70, 1),
                new Vertex(-30, 100, -80, 1),
                new Vertex(-30, -100, -80, 1),
                color));




        tris.add(new Triangle(new Vertex(-30, 100, -80, 1),
                new Vertex(-30, -100, -80, 1),
                new Vertex(-20, -100, -90, 1),
                color));

        tris.add(new Triangle(new Vertex(-30, 100, -80, 1),
                new Vertex(-20, 100, -90, 1),
                new Vertex(-20, -100, -90, 1),
                color));

        tris.add(new Triangle(new Vertex(-20, 100, -90, 1),
                new Vertex(-20, -100, -90, 1),
                new Vertex(-10, -100, -100, 1),
                color));

        tris.add(new Triangle(new Vertex(-20, 100, -90, 1),
                new Vertex(-10, 100, -100, 1),
                new Vertex(-10, -100, -100, 1),
                color));












        return tris;
    }


}
