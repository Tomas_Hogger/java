package figure;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Tetrahedron   {

    static Color color;

    static List<Triangle> tris = new ArrayList<>();


    public static List<Triangle> getFigure() {

        color = Color.RED;

        //A
        tris.add(new Triangle(new Vertex(-100, -100, -100, 1),
                new Vertex(0, -100, 100, 1),
                new Vertex(100, -100, -100, 1),
                color));
        //B
        tris.add(new Triangle(new Vertex(-100, -100, -100, 1),
                new Vertex(100, -100, -100, 1),
                new Vertex(0, 100, 0, 1),
                color));
        //C
        tris.add(new Triangle(new Vertex(-100, -100, -100, 1),
                new Vertex(0, -100, 100, 1),
                new Vertex(0, 100, 0, 1),
                color));
        //D
        tris.add(new Triangle(new Vertex(0, -100, 100, 1),
                new Vertex(100, -100, -100, 1),
                new Vertex(0, 100, 0, 1),
                color));

        return tris;
    }






}
