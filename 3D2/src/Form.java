


import Oper.Pane;
import figure.*;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class Form extends JFrame{
    private JPanel rootPanel;
    private JPanel pane;
    private JButton pyramidButton;
    private JButton cilButton;
    private JButton tetrButton;
    private JButton cubeButton;


    {
        setContentPane(rootPanel);
        setSize(800,600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pane.add(new Pane(Cube.getFigure()));
        setVisible(true);
    }

    public Form() throws InterruptedException {
        
        pyramidButton.addActionListener(e -> refresh(Pyramid.getFigure()));
        cubeButton.addActionListener(e -> refresh(Cube.getFigure()));
        tetrButton.addActionListener(e -> refresh(Tetrahedron.getFigure()));
        cilButton.addActionListener(e -> refresh(Cone.getFigure()));
    }


    private void refresh(List<Triangle> tris){

        pane.removeAll();
        pane.repaint();
        pane.add(new Pane(tris), BorderLayout.CENTER);
        pane.revalidate();
        pane.repaint();

    }

    public static void main(String[] args) throws InterruptedException {
        Form form = new Form();
    }


}
