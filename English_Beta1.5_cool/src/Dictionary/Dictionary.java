package Dictionary;
import java.util.ArrayList;
import java.util.HashMap;

public class Dictionary {

    private static final int MAX_LEVEL = 5;
    private int level;
    private HashMap<Integer,HashMap<String,TranslationWord>> dictionary;


    public Dictionary(){
        dictionary = new HashMap<>();
        dictionary.put(0,new HashMap<>());
    }



    //возвращает из строки класс TranslationWord со значениями: английское слово - перевод[]
    public static TranslationWord convertStringToDictionary(String str){
        String[] q = str.trim().split("-");
        if(q.length < 2){
            return null;
        }
        String key = q[0].trim().toLowerCase();
        String[] value = q[1].split(",");
        for(int i = 0; i < value.length; i++){
            value[i] = value[i].trim();
        }
        return new TranslationWord(key,value);
    }

    //возвращает коллекцию слов, которые не перешли на следующий уровень познания
    public TranslationWord[] getSetOfWordsMinLevel(){
        for(Integer integer : dictionary.keySet()){
            TranslationWord[] translationWords = dictionary.get(integer).values().toArray(new TranslationWord[0]);
            if(translationWords.length > 0){
                level = integer;
                return translationWords;
            }
        }
        return null;
    }


    public int getLevel(){
        return level;
    }

    public void setForgottenStatus(String word) {
        for(int i : dictionary.keySet()){
            for(TranslationWord translationWord : dictionary.get(i).values()){
                if(translationWord.getWord().equals(word)){
                    translationWord.setStatus(TranslationWord.STATUS_FORGOTTEN);
                }
            }
        }
    }




    public ArrayList<String> getKeys(String word){
        ArrayList<String> keys = new ArrayList<>();
        for(Integer integer : dictionary.keySet()) {
            for (TranslationWord translationWord : dictionary.get(integer).values()) {
                for(String key : translationWord.getTranslateWords()){
                    if(key.equals(word)){
                        keys.add(translationWord.getWord());
                    }
                }
            }
        }
        return keys;
    }

    public boolean contains(String str){
        for(int i : dictionary.keySet()) {
            for (TranslationWord translationWord : dictionary.get(i).values()){
                if(translationWord.getWord().equals(str)){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean contains(TranslationWord str) {
        return dictionary.get(0).containsKey(str);
    }

    public TranslationWord getRandomWord(int rand) {
        String[] arrayList =  dictionary.get(0).keySet().toArray(new String[0]);
        return dictionary.get(0).get(arrayList[rand]);
    }

    public void add(TranslationWord translationWord){
        dictionary.get(0).put(translationWord.getWord(),translationWord);
    }

    public void add(String str){
        if(!str.trim().equals("")) {
            str = checkRepeat(str,0);
            TranslationWord dict = convertStringToDictionary(str);
            if(dict != null){
                add(dict);
            }
        }
    }

    //проверяет список на повторение английских слов и,если оно присуствует, то присваивает номер
    private String checkRepeat(String str,int i){
        if(contains(str+i)){
            return (str = checkRepeat(str,i+1));
        }
        return str = str + (i!=0?"("+i+")":"");
    }

    public int sizeZero(){
        return dictionary.get(0).size();
    }

    public HashMap<Integer,HashMap<String,TranslationWord>> getDictionary(){
        return dictionary;
    }

    //перекидывает слово на следующий уровень познания
    public void addLevelWord(TranslationWord translationWord){
        int l = level + 1;
        if(!dictionary.containsKey(l)){
            dictionary.put(l, new HashMap<>());
        }
        dictionary.get(l).put(translationWord.getWord(), dictionary.get(level).remove(translationWord.getWord()));
        if(l == MAX_LEVEL && dictionary.get(level).isEmpty()){
            dictionary.replace(0, dictionary.remove(l));
        }
    }

}

