package Dictionary;

public class TranslationWord {




    public static final int STATUS_NEW = 0, STATUS_FORGOTTEN = 1, STATUS_LEARNED = 3;

    private String word;
    private String[] translateWords;
    private int status;

    public TranslationWord(String engWord, String[] rusWords){
        this.word = engWord;
        this.translateWords = rusWords;
        status = STATUS_NEW;
    }




    public int getStatus() {
        return status;
    }

    public boolean setStatus(int status) {
        if(this.status == STATUS_NEW){
            this.status = status;
            return status == STATUS_LEARNED;
        }
        return false;
    }

    public String[] getTranslateWords() {
        return translateWords;
    }

    public void setTranslateWords(String[] translateWords) {
        this.translateWords = translateWords;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
