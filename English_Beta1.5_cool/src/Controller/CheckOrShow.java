package Controller;

import Dictionary.TranslationWord;

public class CheckOrShow {

    private boolean checkOrShow;
    private String showWord;
    private String checkWord;
    private boolean right;
    private TranslationWord translationWord;
    private TranslationWord originalTranslationWord;


    public CheckOrShow(){
        checkOrShow = false;
    }

    public CheckOrShow(String showWord, TranslationWord translationWord){
        this.checkOrShow = true;
        this.showWord = showWord;
        this.translationWord = translationWord;
    }

    public CheckOrShow(TranslationWord originalTranslationWord, String str) {
        this(str,null);
        this.originalTranslationWord = originalTranslationWord;
    }

    public boolean addCheck(boolean right, String checkWord){
        checkOrShow = false;
        this.checkWord = checkWord;
        this.right = right;
        if(right){
            return translationWord.setStatus(TranslationWord.STATUS_LEARNED);
        } else {
            return translationWord.setStatus(TranslationWord.STATUS_FORGOTTEN);
        }
    }

    public boolean addCheck(boolean right, String checkWord, TranslationWord translationWord){
        this.translationWord = translationWord;
        return addCheck(right,checkWord);
    }


    public boolean isCheckOrShow() {
        return checkOrShow;
    }

    public void setCheckOrShow(boolean checkOrShow) {
        this.checkOrShow = checkOrShow;
    }

    public String getShowWord() {
        return showWord;
    }

    public void setShowWord(String showWord) {
        this.showWord = showWord;
    }

    public String getCheckWord() {
        return checkWord;
    }

    public void setCheckWord(String checkWord) {
        this.checkWord = checkWord;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public TranslationWord getTranslationWord() {
        return translationWord;
    }

    public void setTranslationWord(TranslationWord translationWord) {
        this.translationWord = translationWord;
    }

    public TranslationWord getOriginalTranslationWord() {
        return originalTranslationWord;
    }

    public void setOriginalTranslationWord(TranslationWord originalTranslationWord) {
        this.originalTranslationWord = originalTranslationWord;
    }
}
