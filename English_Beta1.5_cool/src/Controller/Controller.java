package Controller;

import Dictionary.*;

import java.io.*;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.stream.Collectors;


public class Controller {

    private Random random;
    private Dictionary dictionary;
    private Dictionary littleDictionary;
    private CheckOrShow checkOrShow; //проверять или выводить слово
    private boolean engOrRus; //проверять перевод с английского на русский или обратно
    private int numberOfWordsToCheck; //количество слов для отображения
    private int numberOfLearnedWords; //количество изученных слов
    private String charset = Charset.defaultCharset().name(); //кодировка файла
    private String standardNameOfDictionary = "English.txt";
    private File standardFileOfDictionary = new File(System.getProperty("user.dir")+"/"+standardNameOfDictionary);

    public Controller() throws IOException {
        createNewDictionary();
    }

    //устанавливает всем переменным начальное значение
    public void reload(){
        checkOrShow = new CheckOrShow();
        engOrRus = false;
        random = new Random();
        dictionary = new Dictionary();
        littleDictionary = new Dictionary();
        numberOfWordsToCheck = 0;
        numberOfLearnedWords = 0;
    }

    public void createNewDictionary() {

        if(!standardFileOfDictionary.exists()){
            try {
                standardFileOfDictionary.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        reload();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(standardFileOfDictionary), charset));
            reload();
            String str;
            while ((str = bufferedReader.readLine()) != null){
                dictionary.add(str);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public int getSizeDictionary(){
        int i = 0;
        try {
            i = dictionary.sizeZero();
        } catch (Exception e){
            return i;
        }
        return i;
    }

    public boolean isDictionaryNull(){
        return dictionary == null;
    }

    public boolean isCreateWordsToCheck(){
        return numberOfWordsToCheck != 0;
    }

    //метод, создающий коллекциюиию случайных слов, для проверки
    //numbers - колличество слов
    public void createWordsToCheck(String str){

        if(str == null || str.equals("")){
            return;
        }
        int numbers;
        try {
            numbers = Integer.valueOf(str);
        } catch (Exception e){
            return;
        }
        if(numbers > dictionary.sizeZero()){
            numbers = dictionary.sizeZero();
        }
        numberOfWordsToCheck = numbers;
        for(int i = 0; i < numbers; i++){
            int rand = random.nextInt(getSizeDictionary());
            TranslationWord t = dictionary.getRandomWord(rand);
            if(!littleDictionary.contains(t)){
                littleDictionary.add(t);
            } else {
                i--;
            }
        }
    }

    public CheckOrShow checkOrShow(String word){
        if(checkOrShow.isCheckOrShow()){
            check(word);
            return checkOrShow;
        } else {
            checkOrShow = show();
            return checkOrShow;
        }
    }

    //метод, возвращающий класс CheckOrShow с словом для проверки
    private CheckOrShow show() {
        TranslationWord[] arrayList = littleDictionary.getSetOfWordsMinLevel();
        int rand1 = random.nextInt(arrayList.length);
        int rand2 = random.nextInt(2);
        TranslationWord translationWord = arrayList[rand1];
        if (rand2 == 1) {
            String[] strings = translationWord.getTranslateWords();
            int rand3 = random.nextInt(strings.length);
            engOrRus = false;
            String str = strings[rand3];
            return new CheckOrShow(translationWord, str);
        } else {
            engOrRus = true;
            return new CheckOrShow(translationWord.getWord(), translationWord);
        }
    }

    //проверяет слово, введённое пользователем
    //так же присваивает статус словам
    private void check(String word){
        boolean check = false;
        if (engOrRus) {
            String[] values = checkOrShow.getTranslationWord().getTranslateWords();
            for (String strings1 : values) {
                if (strings1.equals(word)) {
                    check = true;
                    break;
                }
            }
            if(checkOrShow.addCheck(check,word)){
                numberOfLearnedWords++;
            }
            if(check){
                littleDictionary.addLevelWord(checkOrShow.getTranslationWord());
            }
        } else {
            ArrayList<String> keys = search(checkOrShow.getShowWord());
            boolean check2 = keys.contains(word);
            if(checkOrShow.addCheck(check2,
                    word,
                    new TranslationWord(word, keys.toArray(new String[0])))){
                numberOfLearnedWords++;
            }
            if(check2) {
                littleDictionary.addLevelWord(checkOrShow.getOriginalTranslationWord());
            } else {
                littleDictionary.setForgottenStatus(word);
            }
        }
    }


    //метод для поиска всех английских слов, которые соответствуют переводу данного пользователю для перевода слову
    private ArrayList<String> search(String word){
        return littleDictionary.getKeys(word);
    }


    //сохраняет забытые слова
    public void saveForgottenWords(File file){
        try (PrintWriter printWriter = new PrintWriter(file, charset)){
            HashMap<Integer,HashMap<String,TranslationWord>> hashMap = littleDictionary.getDictionary();
            StringBuilder stringBuilder = new StringBuilder();
            for(int i : hashMap.keySet()) {
                for (TranslationWord translationWord : hashMap.get(i).values().toArray(new TranslationWord[0])) {
                    if(translationWord.getStatus() == TranslationWord.STATUS_FORGOTTEN) {
                        stringBuilder.append(translationWord.getWord()).append(" - ")
                                .append(Arrays.stream(translationWord.getTranslateWords()).collect(Collectors.joining(",")))
                                .append("\r\n");
                        if (stringBuilder.length() > 512_000) {
                            printWriter.write(stringBuilder.toString());
                            stringBuilder = new StringBuilder();
                        }
                    }
                }
            }
            printWriter.write(stringBuilder.toString());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    public int getLevel(){
        int i = 0;
        try {
            i = littleDictionary.getLevel();
        } catch (Exception e){
            return i;
        }
        return i;
    }

    public int getNumberOfLearnedWords() {
        return numberOfLearnedWords;
    }

    public void setNumberOfLearnedWords(int numberOfLearnedWords) {
        this.numberOfLearnedWords = numberOfLearnedWords;
    }

    public boolean isDictionaryEmpty(){
        return dictionary.sizeZero() > 0;
    }
}
