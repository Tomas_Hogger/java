import Controller.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;


public class English extends JFrame {

    private JPanel rootPanel;
    private JTextArea wordArea;
    private JButton showButton;
    private JTextArea textArea;
    private JLabel infoLabel;
    private JButton settingsButton;
    private Controller controller = new Controller();
    private Settings settings = new Settings(controller);

    private boolean showSettings = false;
    private JPanel settingsPanel = new JPanel(new CardLayout());;


    {
        settingsPanel.add(settings.getRootPanel());
        setLayout(new GridBagLayout());
        setContentPane(rootPanel);
        setSize(800,600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        changeInfo();
        revalidate();
        repaint();
    }

    public English() throws IOException {

        showButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        showButton.getActionMap().put("pressed", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showCheck();
            }
        });
        showButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showCheck();
            }
        });

        wordArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    e.consume();
                    showCheck();
                }
            }
        });


        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(showSettings) {
                    remove(settingsPanel);
                    revalidate();
                    repaint();
                    showSettings = false;
                } else {
                    GridBagConstraints gbc = new GridBagConstraints();
                    gbc.gridx = getWidth();
                    int top = 0;
                    gbc.gridy = top;
                    gbc.gridheight = getHeight();
                    int weight = 4;
                    gbc.weightx = weight;
                    gbc.fill = GridBagConstraints.BOTH;
                    add(settingsPanel, gbc);
                    revalidate();
                    repaint();
                    showSettings = true;
                }
            }
        });



    }



    private void showCheck() {

            if (!controller.isDictionaryNull() && controller.isDictionaryEmpty()) {
                if (controller.isCreateWordsToCheck()) {
                    CheckOrShow checkOrShow = controller.checkOrShow(wordArea.getText().trim());
                    if (checkOrShow.isCheckOrShow()) {
                        //==========
                        //выводит слово на английском или русском языке (выбор рандомный)
                        wordArea.setText("");
                        changeInfo();
                        textArea.setText(checkOrShow.getShowWord());
                        showButton.setText("Check");
                        //===========
                    } else {
                        //===========
                        // проверяет слово, введённое пользователем
                        message(checkOrShow.getCheckWord(),
                                checkOrShow.isRight(),
                                checkOrShow.getTranslationWord().getTranslateWords(),
                                checkOrShow.getShowWord());
                        showButton.setText("Show");
                    }
                    //==============
                } else {
                    controller.createWordsToCheck(JOptionPane.showInputDialog(rootPanel,
                            "Сколько слов проверить?(Максимум " + controller.getSizeDictionary() + ")"));
            }
        } else {
                JOptionPane.showConfirmDialog(rootPanel,"В словарь еще не добавленно ни одного слова.","Сообщение", JOptionPane.DEFAULT_OPTION);
            }
    }

    /*
    выводит результат в textArea
    str - перевод пользователя
    right - верно ли пользователь перевёл данное ему слово
    strings - массив правильных переводов
    text - слово, данное пользователю для перевода
     */
    private void message(String str, boolean right, String[] strings, String text){
        textArea.setText(text + "\n" + str + " is" + (right?" right.":" wrong.") + "\n"
                + Arrays.stream(strings).collect(Collectors.joining(",")));
        changeInfo();
    }

    private void changeInfo(){
        infoLabel.setText( "Level is: " + controller.getLevel() + ".   Learned words: " + controller.getNumberOfLearnedWords() + ".");
    }


    public static void main(String[] args) throws IOException {
        English english = new English();
    }


}
