import Controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class Settings{


    private JPanel rootPanel;
    private JButton addButton;
    private JTextField addWordField;
    private JPanel panelForWords;
    private JTextField searchWordField;
    private JLabel infoLabel;

    private String searchHint = "Search";
    private String addHint = "Add word";

    private Controller controller;


    {
        searchWordField.setForeground(Color.LIGHT_GRAY);
        searchWordField.setText(searchHint);
        addWordField.setForeground(Color.LIGHT_GRAY);
        addWordField.setText(addHint);

    }

    public Settings(Controller controller){
        this.controller = controller;
        infoLabel.setText("Number of words: " + this.controller.getSizeDictionary());

        searchWordField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                searchWordField.setForeground(Color.BLACK);
                if (searchWordField.getText().equals(searchHint)) {
                    searchWordField.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (searchWordField.getText().isEmpty()) {
                    searchWordField.setForeground(Color.LIGHT_GRAY);
                    searchWordField.setText(searchHint);
                }
            }
        });


        addWordField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                addWordField.setForeground(Color.BLACK);
                if (addWordField.getText().equals(addHint)) {
                    addWordField.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (addWordField.getText().isEmpty()) {
                    addWordField.setForeground(Color.LIGHT_GRAY);
                    addWordField.setText(addHint);
                }
            }
        });




    }




    public JPanel getRootPanel() {
        return rootPanel;
    }





}
