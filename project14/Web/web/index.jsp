<%-- 
    Document   : index
    Created on : 28.10.2018, 15:06:42
    Author     : Tim
--%>

<%@page import="java.util.TreeMap"%>
<%@page import="java.util.SortedSet"%>
<%@page import="analyzer.Time.TimePeriod"%>
<%@page import="analyzer.Time.WorkTime"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="analyzer.Provider"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%! private static final DateFormat timeFormat = new SimpleDateFormat("HH:mm");%>
<%!    private static final DateFormat dayFormat = new SimpleDateFormat("dd.MM.yyyy");%>
<% Provider provider = new Provider(getServletContext(), "/WEB-INF/resources/data-0.2M.xml");%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            table,td{
                border : solid;
                border-color: black;
                border-width: 1px;
            }
        </style> 
    </head>
    <body>
        <table>
            <tr>
                <td>Станция№</td>
                <% SortedSet<Date> dates = provider.getVotingDate();%>
                <% for(Date date : dates){%>
                <td>
                <%=dayFormat.format(date)%>
                </td>
                <%}%>
            </tr>
                <% TreeMap<Integer,WorkTime> treeMap = provider.getStation();%>
                <% for(Integer i : treeMap.keySet()){%>
                    <tr>
                        <td>
                            <%=i%>
                        </td>
                        <% HashMap<Date, TimePeriod> periods = treeMap.get(i).getPeriods();%>
                        <% for(Date date : dates){%>
                        <td>
                            <%if(periods.keySet().contains(date)){%>
                                 <%=timeFormat.format(periods.get(date).getFrom())%>
                                 -
                                 <%=timeFormat.format(periods.get(date).getTo())%>
                                 <%}else{%>
                                 Было закрыто.
                               <%}%>
                        </td>
                    <%}%>
                    </tr>
                    <%}%>
        </table>
    </body>
</html>
