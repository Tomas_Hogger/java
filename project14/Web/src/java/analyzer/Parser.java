/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analyzer;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Tim
 */
public class Parser {
    public static Date parse(String _time, SimpleDateFormat visitDateFormat ) throws ParseException{
        ParsePosition parse = new ParsePosition(0);
        _time = _time.trim();
        Date time = visitDateFormat.parse(_time,parse);
        if(time == null || parse.getIndex() < _time.length()){
            throw new ParseException(_time, parse.getIndex());
        }
        return time;
    }
}
