/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analyzer;

import analyzer.Time.WorkTime;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.servlet.ServletContext;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Tim
 */
public class Provider {
    
    private HashMap<Integer,WorkTime> voteStationWorkTimes;  
    
    public Provider(ServletContext context, String path) throws MalformedURLException, IOException, SAXException, ParserConfigurationException{
        URL url = context.getResource(path);
        try(InputStream stream = url.openStream()){
        voteStationWorkTimes = new Handler().parse(stream).getVoteStation();
        }  catch (IOException | SAXException | ParserConfigurationException ex) {
            throw new RuntimeException(ex);
        } 
    }
    
    public SortedSet<Date> getVotingDate(){
        SortedSet<Date> set = new TreeSet<>();
        for(WorkTime workTime : voteStationWorkTimes.values()){
            for(Date date : workTime.getPeriods().keySet())
            set.add(date);
        }
        return set;
    }
     
     public TreeMap<Integer,WorkTime> getStation(){
         return new TreeMap(voteStationWorkTimes);
     }

}
