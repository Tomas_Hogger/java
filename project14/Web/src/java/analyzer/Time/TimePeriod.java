package analyzer.Time;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Danya on 24.02.2016.
 */
public class TimePeriod 
{
    private Date from;
    private Date to;

   

    public TimePeriod(Date from, Date to)
    {
        this.from = from;
        this.to = to;
        SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy.MM.dd");
        if(!dayFormat.format(from).equals(dayFormat.format(to)))
            throw new IllegalArgumentException("Dates 'from' and 'to' must be within ONE day!");
    }

    public void appendTime(Date time)
    {
         if(!to.before(from)) {
            if(from.after(time))
                from = new Date(time.getTime());
            else if(to.before(time))
                to = new Date(time.getTime());
        }
    }

    public Date getTo(){
        return to;
    }
    
    public Date getFrom(){
        return from;
    }
    
}
