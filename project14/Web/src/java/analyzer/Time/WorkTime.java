package analyzer.Time;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 * Created by Danya on 24.02.2016.
 */
public class WorkTime
{
     private HashMap<Date, TimePeriod> periods = new HashMap<>();
        
    public void addVisitTime(Date visitTime) {
        Date day = truncate(visitTime);
        if(periods.containsKey(day)) {
            periods.get(day).appendTime(visitTime);
        } else {
            periods.put(day, new TimePeriod(visitTime,visitTime));
        }
    }
    
    public HashMap<Date, TimePeriod> getPeriods() {
        return periods;
    }
    
    public static Date truncate(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);        
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
