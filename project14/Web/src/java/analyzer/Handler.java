package analyzer;


import analyzer.Time.WorkTime;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;



public class Handler extends DefaultHandler {

    private static SimpleDateFormat visitDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

    private HashMap<Integer,WorkTime> voteStationWorkTimes = new HashMap<>();    

    @Override
    public void startElement(String uri, String localName, String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
            if(qName.equals("visit")){
            Integer station = Integer.parseInt(attributes.getValue("station"));
            Date time = null;
            try {
                time = Parser.parse(attributes.getValue("time"), visitDateFormat);
            } catch (ParseException ex) {
                throw new RuntimeException(ex);
            }
            WorkTime workTime = voteStationWorkTimes.get(station);
            if(workTime == null)
            {
                workTime = new WorkTime();
                voteStationWorkTimes.put(station, workTime);
            }
            workTime.addVisitTime(time);
        }
    }

    public Handler parse(InputStream stream) throws SAXException, IOException, ParserConfigurationException{
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        parser.parse(stream, this);
        return this;
    }
    
    public HashMap<Integer,WorkTime> getVoteStation(){
        return voteStationWorkTimes;
    }
    

}

