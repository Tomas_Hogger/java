public class Loader {
    public static void main(String[] args) {
        Integer moneyAmount = 50000;
        Boolean hasPreviousVisa = false;
        Boolean hasConviction = true;
        if((hasPreviousVisa || moneyAmount > 45000) && !hasConviction){
            System.out.println("Visa should be granted");
        }
        else {
            System.out.println("Visa cannot be granted");
        }
    }
}
