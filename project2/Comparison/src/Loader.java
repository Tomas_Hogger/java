public class Loader {
    public static void main(String[] args) {
        Integer dimaAge = 35;
        Integer mishaAge = 18;
        Integer vasyaAge = 35;

        Integer oldest;
        Integer youngest;
        Integer middle = null;

        if (dimaAge > mishaAge) {
            if (dimaAge > vasyaAge) {
                oldest = dimaAge;
                if (vasyaAge > mishaAge) {
                    middle = vasyaAge;
                    youngest = mishaAge;
                } else {
                    youngest = vasyaAge;
                    middle = mishaAge;
                }
            } else {
                oldest = vasyaAge;
                youngest = mishaAge;
                middle = dimaAge;
            }
        } else {
            if (vasyaAge > mishaAge) {
                oldest = vasyaAge;
                youngest = dimaAge;
                middle = mishaAge;
            } else {
                oldest = mishaAge;
                if (vasyaAge > dimaAge) {
                    youngest = dimaAge;
                    middle = vasyaAge;
                } else {
                    youngest = vasyaAge;
                    middle = dimaAge;
                }
            }
        }
        System.out.println("Most old:" + oldest);
        System.out.println("Middle:" + middle);
        System.out.println("Most young:" + youngest);
    }
}
