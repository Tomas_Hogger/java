
package downloader;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tim
 */
public class FileHelper {

    public static File getFile(ServletContext servletContext) {
        String g = servletContext.getInitParameter("FilePath");
        return new File(g);
    }

    public static void checkFilePath(File file) throws IOException {
        if(!file.exists()){
            if(file.mkdirs()){
                throw new IOException("Cannot create FilePath");
            }
        } else if(!file.isDirectory()){
            throw new IOException("FilePath isn't a directory!");
        }
    }
    
    public static List<File> getFiles(ServletContext servletContext){
        List<File> files1 = new ArrayList<>(); 
        File filePath = getFile(servletContext);
        
        
        if(!filePath.exists()){
            return files1;
        }
        
        File[] files2 = filePath.listFiles();
        
        if(files2 == null){
            return files1;
        }
        
        
        for(File file : files2){
            if(file.isFile()){
                files1.add(file);
            }
        }
        
        return files1;
        
    }
    
}
