package downloader;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import downloader.FileHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Tim
 */
@WebServlet(urlPatterns = {"/upload.jsp"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2,
        maxFileSize = 1024 * 1024 * 10,
        maxRequestSize = 1024 * 1024 * 50)
public class UploadServlet extends HttpServlet {

    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        try{
            File saveDir = FileHelper.getFile(getServletContext());
            FileHelper.checkFilePath(saveDir);
            
            if (!request.getContentType().contains("multipart/form-data")) {
                throw new IOException("Wrong request type!");
            }
            
            for(Part part : request.getParts()){
                String fileName = part.getSubmittedFileName();
                
                if(fileName == null || !part.getName().equals("file")){
                    continue;
                }
                
                File file = new File(saveDir , fileName);
                
                if(!saveDir.equals(file.getParentFile())){               
                    throw new IllegalArgumentException("Access rights violation");
                }
                
                try (InputStream input = part.getInputStream();
                
                        OutputStream output = new FileOutputStream(file)) {
                        //response.setContentLength((int)file.length());
                        byte[] buf = new byte[4096];
                        int length;
                        while ((length = input.read(buf)) >= 0) {
                            output.write(buf, 0, length);
                        }
                    }
                break;
            }
            
            response.sendRedirect("index.jsp");
        } catch (Exception e){                     
            getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");

        }
        
        
        
        
    }
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
