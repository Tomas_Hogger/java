<%-- 
    Document   : index
    Created on : 14.11.2018, 21:36:08
    Author     : Tim
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="downloader.FileHelper"%>
<%@page import="java.io.File"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    request.setAttribute("files", FileHelper.getFiles(getServletContext()));
%>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Загрузка файла</h1>
        <form action="upload.jsp" method="post" enctype="multipart/form-data">
            <input type="file" name="file"><br><br>
            <input type="submit" value="Загрузить">
        </form><br>
        
        <ul>
            <%for(File file : (ArrayList<File>) request.getAttribute("files")){ %>
            <li><a href="download.jsp?file=<%=file.getName()%>">Загрузить <%=file.getName()%></a></li>
            <%}%>            
        </ul>
        
        
    </body>
</html>
