/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package counter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tim
 */
public class Browsers {
    
    
    private List<Browser> browsers = new ArrayList<>();
    private int requestTotal;
   
    
    public int getRequestTotal() {
        return requestTotal;
    }

    public void setRequestTotal(int requestTotal) {
        this.requestTotal = requestTotal;
    }

    public List<Browser> getBrowsers() {
        return browsers;
    }

    public void setBrowsers(List<Browser> browsers) {
        this.browsers = browsers;
    }
    
}
