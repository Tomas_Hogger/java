/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package counter;

/**
 *
 * @author Tim
 */
public class Browser {
    
    private String name;
    private int requestCount;
    private double requestPercent;
    
    public Browser(String name, int requestCount){
        this.name = name;
        this.requestCount = requestCount;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public double getRequestPercent() {
        return requestPercent;
    }

    public void setRequestPercent(double requestPercent) {
        this.requestPercent = requestPercent;
    }
    
    
    
}
