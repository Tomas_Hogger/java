/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package counter;

import counter.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author Tim
 */
public class Manager {
    
   
    
    public static String getBrowserName(String str){
        String name;
        if (str.contains("Chrome"))
            name = "Google Chrome or Chromium-based";
        else if (str.contains("Opera") || str.contains("Presto"))
            name = "Opera";
        else if (str.contains("Firefox"))
            name = "Mozilla Firefox";
        else if (str.contains("MSIE") || str.contains("Trident"))
            name = "Internet Explorer";
        else
            name = "Other browsers";
        return name;
    }

    static Browsers getBrowsers(String browserName) throws SQLException {

        Connection connection = DBConnection.getConnection();
            
        if(browserName != null){
            try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO visit(name, count) VALUES(?, 1) "
                + " ON DUPLICATE KEY UPDATE count = count + 1;"
            )){

                statement.setString(1, browserName);
                statement.execute();
            }
        }
            
        Browsers browsers = new Browsers();
        int sum = 0;
        
        try(Statement statement = connection.createStatement()){
            try (ResultSet set = statement.executeQuery("SELECT name name, count count FROM visit")){
                while(set.next()){
                    String s = set.getString("name");
                    int l = set.getInt("count");
                    browsers.getBrowsers().add(new Browser(s,l));
                    sum += l;
                }
            }
        }
        
        for(Browser browser : browsers.getBrowsers()){        
            browser.setRequestPercent(browser.getRequestCount() * 100.0 / sum);
        }
        
        browsers.setRequestTotal(sum);
        return browsers;
        
    }
    
    
    
    
}
