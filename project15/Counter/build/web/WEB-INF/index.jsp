<%-- 
    Document   : index
    Created on : 20.11.2018, 19:51:24
    Author     : Tim
--%>

<%@page import="counter.Browser"%>
<%@page import="counter.Browsers"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%Browsers browsers = (Browsers) session.getAttribute("browsers"); %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table border="1">
            <%for(Browser browser : browsers.getBrowsers()){%>
            <tr>
                <td><%=browser.getName()%></td>
                <td><%=browser.getRequestCount()%> раз</td>
                <td><%=String.format("%.2f",browser.getRequestPercent())%>%</td>
            </tr>
            <%}%>
        </table>
        <p>Количество зафиксированных браузеров: <%=browsers.getRequestTotal()%></p>
    </body>
</html>
