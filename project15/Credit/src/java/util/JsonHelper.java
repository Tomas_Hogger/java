/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import entities.Client;
import entities.ClientRequest;
import org.json.simple.JSONObject;

/**
 *
 * @author Tim
 */
public class JsonHelper {
    
    
    public static JSONObject toJSON(Client client) {
        JSONObject object = new JSONObject();
        
        object.put("age", client.getAge());
        object.put("name", client.getName());
        object.put("patronym", client.getPatronym());
        object.put("surname", client.getSurname());
        object.put("salary", client.getSalary());        
        object.put("email", client.getEmail());
        object.put("phone", client.getPhone());
        return object;
    }
    
    public static JSONObject toJSON(ClientRequest clientRequest) {
        JSONObject object = new JSONObject();
        
        object.put("client", toJSON(clientRequest.getClient()));
        object.put("amount", clientRequest.getAmount());
        object.put("term", clientRequest.getTerm());
        object.put("approved", clientRequest.isApproved());
        
        return object;
    }
}
