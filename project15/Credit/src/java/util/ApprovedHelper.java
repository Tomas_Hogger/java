/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import entities.ClientRequest;

/**
 *
 * @author Tim
 */
public class ApprovedHelper {
    
    private static final int MIN_AGE = 18;
    private static final int HALF_SALARY = 10;
    public static boolean getApproved(ClientRequest clientRequest){
        return clientRequest.getClient().getAge() >= MIN_AGE 
                && (clientRequest.getClient().getSalary()/HALF_SALARY*clientRequest.getTerm())>= clientRequest.getAmount();
                
    }
    
}
