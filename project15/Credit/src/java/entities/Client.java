/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Tim
 */
public class Client {

    
    private static final String[] list = {"surname","name","patronym","age","salary","phone","email"}; 

    
    private String surname;
    private String name;
    private String patronym;
    private int age;
    private int salary;   
    private String  phone;
    private String email;
    
    static String[] getList() {
        return list;
    }

    
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronym() {
        return patronym;
    }

    public void setPatronym(String patronym) {
        this.patronym = patronym;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
        
    public Client parseRequest(HttpServletRequest request){
        surname = request.getParameter("surname");
        name = request.getParameter("name");
        patronym = request.getParameter("patronym");
        age = Integer.parseInt(request.getParameter("age"));
        salary = Integer.parseInt(request.getParameter("salary"));
        phone = request.getParameter("phone");
        email = request.getParameter("email");
        return this;
    }
    
    
    static Client getFullClient(Map<String, String> map) {
        Client client = new Client();
        client.age = Integer.valueOf(map.get("age"));
        client.email = Objects.requireNonNull(map.get("email"));
        client.name = Objects.requireNonNull(map.get("name"));
        client.patronym = Objects.requireNonNull(map.get("patronym"));
        client.phone = Objects.requireNonNull(map.get("phone"));
        client.salary = Integer.valueOf(map.get("salary"));
        client.surname = Objects.requireNonNull(map.get("surname"));
      
        return client;             
    }
    
}
