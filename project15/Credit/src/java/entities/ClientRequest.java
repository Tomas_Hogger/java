/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import util.ApprovedHelper;

/**
 *
 * @author Tim
 */
public class ClientRequest {
    
    private static final String[] list = {"amount","term"}; 
    
    private Client client;
    private int amount;
    private int term;
    private boolean approved;
    
    public ClientRequest(){
        client = new Client();
    }
    
    public static String[] getList(){
        String[] list1 = Client.getList();
        String[] list2 = new String[list1.length + list.length];
        int i = 0;
        for(;i < list.length; i++){
            list2[i] = list1[i];
        }
        for(int c = 0; c < list.length; c++, i++){
            list2[i] = list[c];
        }
        return list2;
    }
    
     public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
    
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    
    public ClientRequest parseRequest(HttpServletRequest request){
        client = client.parseRequest(request);
        amount = Integer.parseInt(request.getParameter("amount"));
        term = Integer.parseInt(request.getParameter("term"));
        approved = ApprovedHelper.getApproved(this);
        return this;
    }
    
    
    public static ClientRequest getFullClientRequest(Map<String,String> map){
        ClientRequest client = new ClientRequest();
        client.amount = Integer.valueOf(map.get("amount"));
        client.term = Integer.valueOf(map.get("term"));
        client.client = Client.getFullClient(map);
        client.approved = ApprovedHelper.getApproved(client);
        
        return client;
    }
    
    public Map<String,String> getAll(){
        Map<String,String> map = new TreeMap<>();
        map.put("amount", String.valueOf(amount));
        map.put("term", String.valueOf(term));
        map.put("surname", client.getSurname());
        map.put("name", client.getName());
        map.put("patronym", client.getPatronym());
        map.put("age", String.valueOf(client.getAge()));
        map.put("salary", String.valueOf(client.getSalary()));
        map.put("phone", client.getPhone());
        map.put("email", client.getEmail());
        
        return map;
    }
    
    
}
