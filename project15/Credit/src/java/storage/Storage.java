/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storage;

import entities.ClientRequest;

/**
 *
 * @author Tim
 */
public interface Storage {
    
    void setData(ClientRequest clientRequest);
    ClientRequest getData();
    
}
