/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storage;

import entities.ClientRequest;
import java.util.Map;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tim
 */
public class SessionStorage implements Storage{

    private HttpSession session;
    
    public SessionStorage(HttpSession session){
        this.session = session;
    }
    public void setData(ClientRequest clientRequest) {
        session.setAttribute("clientRequest", clientRequest);
    }

    public ClientRequest getData(){
        return (ClientRequest) session.getAttribute("clientRequest");
    }
    
  
    
}
