/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storage;

import entities.ClientRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tim
 */
public class CookieStorage implements Storage {
    
    private HttpServletRequest request;
    private HttpServletResponse response;
    
    public CookieStorage(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
    }
    
    public void setData(ClientRequest clientRequest){
        Map<String,String> map = clientRequest.getAll();
        for(String str : map.keySet()){
            response.addCookie(new Cookie(str,map.get(str)));
        }     
    }
    
    public ClientRequest getData(){
        Map<String,String> map = Arrays.stream(request.getCookies()).collect(Collectors.toMap(c -> c.getName(), c -> decode(c.getValue()), 
                (a,b) -> a +"," + b, HashMap<String, String>::new));
        boolean check = true;
        for(String str : ClientRequest.getList()){
            if(!map.containsKey(str)){
                check = false;
                break;
            }
        }
        if(check){
            return ClientRequest.getFullClientRequest(map);
        } else {
            return null;
        }
    }
    
    public static String decode(String text) {
        try {
            return URLDecoder.decode(text, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }
    
}
