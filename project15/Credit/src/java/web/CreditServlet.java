/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import analyst.Analyst;
import entities.ClientRequest;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import storage.CookieStorage;
import storage.SessionStorage;
import storage.Storage;
import util.JsonHelper;

/**
 *
 * @author Tim
 */
@WebServlet(name = "CreditServlet", urlPatterns = {"/json.jsp"})
public class CreditServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("index.jsp");
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
                request.setCharacterEncoding("UTF-8");
                Analyst analyst = getAnalyst(request, response).parseRequest(request);
    
                ClientRequest clientRequest = analyst.getClientRequest();
                response.setContentType("application/json; charset = UTF-8");
        try(OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream(), "UTF-8")) {
            JsonHelper.toJSON(clientRequest).writeJSONString(writer);
        }
              
        
    }
    

    public static Analyst getAnalyst(HttpServletRequest request, HttpServletResponse response){
        Storage storage = new CookieStorage(request,response);
        //Storage storage = new SessionStorage(request.getSession());
        return new Analyst(storage);
    }
    

}
