/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analyst;

import entities.ClientRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import storage.Storage;

/**
 *
 * @author Tim
 */
public class Analyst {
    
    
    private ClientRequest clientRequest;
    private Storage storage;


    public Analyst(Storage storage) {
        this.storage = storage;
        clientRequest = new ClientRequest();
    }
    
    
    public Analyst parseRequest(HttpServletRequest request){
        clientRequest = clientRequest.parseRequest(request);
        storage.setData(clientRequest);
        return this;
    }
    
    
    public ClientRequest getClientRequest() {
        return clientRequest;
    }
    
 
    public ClientRequest loadFromStorage(){
        return storage.getData();
    }
    
}
