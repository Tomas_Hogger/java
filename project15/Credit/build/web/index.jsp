
<%@page import="entities.ClientRequest"%>
<%@page import="web.CreditServlet"%>
<!DOCTYPE html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
	request.setCharacterEncoding("UTF-8");
	ClientRequest clientRequest = CreditServlet.getAnalyst(request, response).loadFromStorage();
%>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h2>Заявление на кредит</h2>
        <form method="post" action="json.jsp">
            <b>Фамилия </b><input type="text" name="surname" required value="<%=(clientRequest != null?clientRequest.getClient().getSurname():"")%>"><br><br>
            <b>Имя </b><input type="text" name="name" required value="<%=(clientRequest != null?clientRequest.getClient().getName():"")%>"><br><br>
            <b>Отчество </b><input type="text" name="patronym" required value="<%=(clientRequest != null?clientRequest.getClient().getPatronym():"")%>"><br><br>
            <b>Возраст </b><input type="number" name="age" placeholder="полных лет" min="1" required value="<%=(clientRequest != null?clientRequest.getClient().getAge():"")%>"><br><br>
            <b>Сумма кредита </b><input type="number" name="amount"  min="10000" max="10000000" step="1000" placeholder="в рублях" required value="<%=(clientRequest != null?clientRequest.getAmount():"")%>"><br><br>
            <b>Срок выдачи(кол-во месяцев) </b><input type="number" name="term" min="3" max="36" required value="<%=(clientRequest != null?clientRequest.getTerm():"")%>"><br><br>
            <b>Ваш ежемесячный доход </b><input type="number" name="salary"  min="20000" max="1000000" step="1000" placeholder="в рублях" required value="<%=(clientRequest != null?clientRequest.getClient().getSalary():"")%>"><br><br>
            <b>Номер телефона(без +7) </b><input type="tel" name="phone" minlength="9" required value="<%=(clientRequest != null?clientRequest.getClient().getPhone():"")%>"><br><br>
            <b>Email </b><input type="email" name="email" required value="<%=(clientRequest != null?clientRequest.getClient().getEmail():"")%>"><br><br>
            <input type="submit" />
        </form>
    </body>
</html>