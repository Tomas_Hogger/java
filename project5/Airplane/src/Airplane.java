import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class Airplane {
    public static void main(String[] args) {
        Deque<String> deque = new ArrayDeque<>();
        Scanner scanner = new Scanner(System.in);
        String numberOrCommand;

        while(true){
            System.out.print("Номер самолёта или спец.команда:");
            numberOrCommand = scanner.nextLine();
            int size = deque.size();

            if(numberOrCommand.equals("exitAll")){
                if(!deque.isEmpty()) {
                    for (String b : deque) {
                        System.out.println(b);
                    }
                } else {
                    System.out.println("Самолётов нет");
                }
                continue;
            }


            if(numberOrCommand.equals("exitLast")){
                System.out.println(!deque.isEmpty()?deque.pollLast():"Самолётов нет");
                continue;
            }


            if(size < 5) deque.addLast(numberOrCommand);
             else System.out.println("Нет мест");
        }
    }
}
