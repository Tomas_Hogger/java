import java.util.*;

public class Blat {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<String> arrayList1 = new ArrayList<>();
        HashSet<String> hashSet = new HashSet<>();
        TreeSet<String> treeSet = new TreeSet<>();

        char[] allowedSymbols = {'А', 'В', 'С', 'Е', 'Н', 'К', 'М', 'О', 'Р',
                'Т', 'У', 'Х'};

        for (int i = 111; i <= 999; i += 111) {
            for (Character a : allowedSymbols) {
                for (Character b : allowedSymbols) {
                    for (Character c : allowedSymbols) {
                        for (int k = 1; k <= 95; k++) {
                            String number = (a.toString() + i + b.toString() + c.toString() + (k < 10 ? "0" + k : k));
                            arrayList.add(number);
                            arrayList1.add(number);
                            hashSet.add(number);
                            treeSet.add(number);
                        }
                    }
                }
            }
        }
        Collections.sort(arrayList1);
        Scanner scanner = new Scanner(System.in);
        String word;
        long time;

        while(true){
            System.out.print("Номер автомобиля:");
            word = scanner.nextLine();

            time = System.nanoTime();
            System.out.println("ArrayList:" + arrayList.contains(word) + "(" +(System.nanoTime() - time) + "ns)");

            time = System.nanoTime();
            System.out.println("ArrayListSort:" + (Collections.binarySearch(arrayList1,word)>0) + "(" +(System.nanoTime() - time) + "ns)");

            time = System.nanoTime();
            System.out.println("HashSet:" + hashSet.contains(word) + "(" +(System.nanoTime() - time) + "ns)");

            time = System.nanoTime();
            System.out.println("TreeSet:" + treeSet.contains(word) + "(" +(System.nanoTime() - time) + "ns)");
        }

    }
}
