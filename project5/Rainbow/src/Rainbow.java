public class Rainbow {
    public static void main(String[] args) {
        //var 1
        String colorOfRainbow[] = {"red","orange","yellow","green","blue","dark-blue","purple"};
        for(int i = colorOfRainbow.length - 1; i >= 0; i--){
            System.out.println(colorOfRainbow[i]);
        }

        System.out.println("\n\n\n");

        //var 2
        String repository;
        int arrayLength = colorOfRainbow.length - 1;
        for(int i = arrayLength; i >= arrayLength/2; i --){
            repository = colorOfRainbow[i];
            colorOfRainbow[i] = colorOfRainbow[arrayLength - i];
            colorOfRainbow[arrayLength - i] = repository;
        }

        for (String i : colorOfRainbow){
            System.out.println(i);
        }
    }
}
