import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Medicines {
    public static void main(String[] args) {
        Set<String> medicines = new HashSet<>();
        Scanner scanner = new Scanner(System.in);
        String medicineOrList;

        while(true){
            System.out.print("Название препарата или спец.команда:");
            medicineOrList = scanner.nextLine();


            if(medicineOrList.equals("LIST")){
                if(!medicines.isEmpty()){
                    for(String name: medicines){
                        System.out.println(name);
                    }
                } else {
                    System.out.println("Список пуст.");
                }
                continue;
            }

            medicines.add(medicineOrList);
        }
    }
}
