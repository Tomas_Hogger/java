import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneBook {
    public static void main(String[] args) {
        HashMap<String,String> hashMap = new HashMap<>();
        Scanner scanner = new Scanner(System.in);
        String nameOrNumberOrCommand;
        String number;
        String name;

        while(true){
            System.out.print("Имя,номер или спец.команда:");
            nameOrNumberOrCommand = scanner.nextLine();

            if(nameOrNumberOrCommand.equals("LIST")){
                if(!hashMap.isEmpty()){
                    ArrayList<String> arrayList = new ArrayList<>();
                    for(String num: hashMap.keySet()){
                        arrayList.add(hashMap.get(num)+ "=>" + num);
                    }
                    Collections.sort(arrayList);
                    for(String k : arrayList){
                        System.out.println(k);
                    }
                    continue;
                } else {
                    System.out.println("Книга пуста.");
                    continue;
                }
            }

            if(isName(nameOrNumberOrCommand)) {
                if (hashMap.containsValue(nameOrNumberOrCommand)) {
                    for (String num : hashMap.keySet()) {
                        if (hashMap.get(num).equals(nameOrNumberOrCommand)) {
                            System.out.println(nameOrNumberOrCommand + "=>" + num);
                        }
                    }
                } else {
                    System.out.print("Данного имени не найдено. Напиши номер:");
                    number = scanner.nextLine();
                    if(hashMap.containsKey(number)){
                        System.out.print("Данный номер уже существует. Заменить имя? (YES/NO):");
                        String answer = scanner.nextLine();
                        if(answer.equals("YES")){
                            hashMap.replace(number,nameOrNumberOrCommand);
                        }
                    } else {
                        hashMap.put(number, nameOrNumberOrCommand);
                    }
                }
            } else if (isNumber(nameOrNumberOrCommand)){
                name = hashMap.get(nameOrNumberOrCommand);
                if(name != null){
                    System.out.println(name + "=>" + nameOrNumberOrCommand);
                } else {
                    System.out.print("Данного номера не найдено. Напишите имя:");
                    name = scanner.nextLine();
                    if(isName(name)) {
                        hashMap.put(nameOrNumberOrCommand, name);
                    } else {
                        System.out.println("Некоректный ввод");
                    }
                }
            } else {
                System.out.println("Некоректный ввод.");
            }
        }
    }




    static boolean isName(String name){
        Pattern pattern = Pattern.compile("[A-Za-z]*[a-z]+");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    static boolean isNumber(String number){
        Pattern pattern = Pattern.compile("[0-9]+");
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }
}
