import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

public class NameAndNumber {
    public static void main(String[] args) {
        HashMap<String,String> hashMap = new HashMap<>();
        Scanner scanner = new Scanner(System.in);
        String number;
        String name;
        String isExist;

        while(true){
            System.out.print("Номер автомобиля:");
            number = scanner.nextLine();
            isExist = hashMap.get(number);
            if(isExist != null){
                System.out.println(number + "=>" + isExist);
            } else {
                System.out.print("Такого номера не найдено. Введите владельца данного автомобиля:");
                name = scanner.nextLine();
                hashMap.put(number,name);
            }
        }
    }
}
