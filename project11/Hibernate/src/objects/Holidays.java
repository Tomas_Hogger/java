package objects;

import java.util.Date;

public class Holidays {
    private Integer id;
    private Date startHolidays;
    private Date stopHolidays;
    private Employee employeeId;


    public Holidays(){

    }

    public Holidays(Employee employeeId, Date startHolidays, Date stopHolidays){
        this.employeeId = employeeId;
        this.startHolidays = startHolidays;
        this.stopHolidays = stopHolidays;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartHolidays() {
        return startHolidays;
    }

    public void setStartHolidays(Date startHolidays) {
        this.startHolidays = startHolidays;
    }

    public Date getStopHolidays() {
        return stopHolidays;
    }

    public void setStopHolidays(Date stopHolidays) {
        this.stopHolidays = stopHolidays;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }
}
