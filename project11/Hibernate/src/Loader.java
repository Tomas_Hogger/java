import objects.Department;
import objects.Employee;
import objects.Holidays;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.Projections;

import java.io.File;
import java.sql.SQLOutput;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Danya on 26.10.2015.
 */
public class Loader
{
    final static int AFTER = 1, BEFOR = 2, EQUALS = 3;
    private static SessionFactory sessionFactory;

    public static void main(String[] args) {
        setUp();

        // create a couple of events...
        Session session = sessionFactory.openSession();
        session.beginTransaction();

//        List<Department> departments = (List<Department>) session.createQuery("FROM Department").list();
//        for(Department department : departments) {
//            System.out.println(department.getName());
//        }

//        Department dept = new Department("Отдел производства");
//        session.save(dept);
//        System.out.println(dept.getId());

//        Department dept = (Department) session.createQuery("FROM Department WHERE name=:name")
//            .setParameter("name", "Отдел производства").list().get(0);
//        session.delete(dept);

        System.out.println("Работают не на своём месте:");
        {
            List<Employee> names = (List<Employee>) session.createQuery("SELECT e FROM Employee e JOIN FETCH e.ledDepartment WHERE e.ledDepartment.id != e.department.id").list();
            for (Employee name : names) {
                System.out.println(name.getName());
            }
        }

        System.out.println();
        System.out.println("Зарплата ниже 115_000 рублей:");

        {
            List<Employee> names = (List<Employee>) session.createQuery("SELECT e FROM Employee e JOIN e.ledDepartment WHERE e.salary < 115000").list();
            for (Employee name : names) {
                System.out.println(name.getName());
            }
        }

        System.out.println();
        System.out.println("Устроились до 01.03.2010:");

        {
            List<Employee> names = (List<Employee>) session.createQuery("SELECT e FROM Employee e JOIN e.ledDepartment WHERE e.hireDate < '2010-03-01'").list();
            for (Employee name : names) {
                System.out.println(name.getName());
            }
        }


        {
            if (Long.parseLong(session.createCriteria(Holidays.class).setProjection(Projections.rowCount()).uniqueResult().toString()) == 0) {
                List<Employee> nameIDs = (List<Employee>) session.createQuery("FROM Employee").list();
                for (Employee nameID : nameIDs) {
                    final long STEP = 1814400000;
                    final long YEAR_IN_MIL = 31536000000L;
                    long start = System.currentTimeMillis() + randBetween(1, YEAR_IN_MIL);
                    long stop = start + STEP;
                    Date startHolidays = new Date(start);
                    Date stopHolidays = new Date(stop);
                    Holidays holidays = new Holidays(nameID, startHolidays, stopHolidays);
                    session.save(holidays);
                }
            }
        }

        System.out.println();
        System.out.println("Пересечения отпусков:");
        {
            List<Object[]> holidays = (List<Object[]>) session.createQuery("FROM Holidays h1, " +
                    "Holidays h2 WHERE h1.startHolidays <= h2.stopHolidays " +
                    "AND h2.startHolidays <= h1.stopHolidays AND h1.id != h2.id " +
                    "AND h1.employeeId.department = h2.employeeId.department").list();

            for(Object[] holiday : holidays){
                Holidays holidays1 = (Holidays) holiday[0];
                Holidays holidays2 = (Holidays) holiday[1];
                System.out.println(holidays1.getEmployeeId().getDepartment().getName() + ":");
                System.out.println(holidays1.getEmployeeId().getName() + " from " + holidays1.getStartHolidays() + " to " + holidays1.getStopHolidays());
                System.out.println(holidays2.getEmployeeId().getName() + " from " + holidays2.getStartHolidays() + " to " + holidays2.getStopHolidays());
                System.out.println();
            }
        }

        session.getTransaction().commit();
        session.close();

        //==================================================================
        if ( sessionFactory != null ) {
            sessionFactory.close();
        }
    }

    //=====================================================================

    private static void setUp()
    {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure(new File("src/config/hibernate.cfg.xml")) // configures settings from hibernate.config.xml
                .build();
        try {
            sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
        }
        catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            e.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }


    private static int checkDate(Date date1, Date date2){
        if(date1.equals(date2)){
            return EQUALS;
        } else if (date1.after(date2)){
            return AFTER;
        } else if(date1.before(date2)){
            return BEFOR;
        } else {
            throw new NullPointerException();
        }
    }

    private static long randBetween(long start, long end) {
        return start + (long) Math.round(Math.random() * (end - start));
    }
}
