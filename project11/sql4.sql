SELECT learn.department.name, COUNT(learn.employee.id) number_employees
FROM learn.employee RIGHT JOIN learn.department ON learn.employee.department_id = learn.department.id
GROUP BY learn.department.id