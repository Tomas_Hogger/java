

import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Loader {
    public static void main(String[] args) throws Exception {
        //Reading file to the String
        String text = new String(Files.readAllBytes(Paths.get("res/text_01.txt")));

        TextAnalyzer analyzer = new TextAnalyzer(text);
        System.out.println("Most frequent word: " + analyzer.getMostFrequentWord());


    }


    @Test
    public void getWordTest1() {
        String text = "America's, music, culture, would, be, incomplete, without, blues, music. ";
        TextAnalyzer analyzer = new TextAnalyzer(text);

        String onlyWord[] = analyzer.getWords().toArray(new String[0]);
        String rightOnlyWord[] = {"america's", "music", "culture", "would", "be",
                "incomplete", "without", "blues", "music"};

        Assert.assertArrayEquals(rightOnlyWord, onlyWord);
    }

    @Test(expected = NullPointerException.class)
    public void getWordTest2() {
        String text = null;
        TextAnalyzer analyzer = new TextAnalyzer(text);

        String onlyWord[] = analyzer.getWords().toArray(new String[0]);
        String rightOnlyWord[] = {""};

        Assert.assertArrayEquals(rightOnlyWord, onlyWord);
    }




 /*   @Test(expected = AssertionError.class)
        public void getWordTest2() {
        String text = "email@mail.com";
        TextAnalyzer analyzer = new TextAnalyzer(text);

        String onlyWord[] = analyzer.getWords().toArray(new String[0]);
        String rightOnlyWord[] = {""};

        Assert.assertArrayEquals(rightOnlyWord, onlyWord);
    }
*/





    @Test
    public void getMostFrequentWordTest1() {
        String text = "Americas, music, culture, would, be, incomplete, without, blues, music. ";
        TextAnalyzer analyzer = new TextAnalyzer(text);

        String mostFrequentWord = analyzer.getMostFrequentWord();
        String rightMostFrequentWord = "music";

        Assert.assertEquals(rightMostFrequentWord, mostFrequentWord);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getMostFrequentWordTest2() {
        String text = "";
        TextAnalyzer analyzer = new TextAnalyzer(text);

        String mostFrequentWord = analyzer.getMostFrequentWord();
        String rightMostFrequentWord = "";

        Assert.assertEquals(rightMostFrequentWord, mostFrequentWord);
    }

    @Test(expected = NullPointerException.class)
    public void getMostFrequentWordTest3() {
        String text = null;
        TextAnalyzer analyzer = new TextAnalyzer(text);

        String mostFrequentWord = analyzer.getMostFrequentWord();
        String rightMostFrequentWord = "";

        Assert.assertEquals(rightMostFrequentWord, mostFrequentWord);
    }



  /*  @Test(expected = ComparisonFailure.class)
    public void getMostFrequentWordTest3() {
        String text = "Americas, music, culture, would, be, incomplete, without, blues.";
        TextAnalyzer analyzer = new TextAnalyzer(text);

        String mostFrequentWord = analyzer.getMostFrequentWord();
        String rightMostFrequentWord = "";

        Assert.assertEquals(rightMostFrequentWord, mostFrequentWord);
    }
    */






}