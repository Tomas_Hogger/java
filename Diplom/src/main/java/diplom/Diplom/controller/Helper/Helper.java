package diplom.Diplom.controller.Helper;

import diplom.Diplom.entities.Weather;

public class Helper {

    public static WeatherHelper convert(Weather weather){

        WeatherHelper weatherHelper1 = new WeatherHelper();

        weatherHelper1.setHumidity(weather.getHumidity());
        weatherHelper1.setPressure(weather.getPressure());
        weatherHelper1.setSunrise(weather.getSunrise());
        weatherHelper1.setSunset(weather.getSunset());
        weatherHelper1.setTemp(weather.getTemp());
        weatherHelper1.setText(weather.getText());
        weatherHelper1.setTime(weather.getTime());
        weatherHelper1.setVisibility(weather.getVisibility());
        weatherHelper1.setWindChill(weather.getWindChill());
        weatherHelper1.setWindDirection(weather.getWindDirection());
        weatherHelper1.setWindSpeed(weather.getWindSpeed());

        return weatherHelper1;
    }


}
