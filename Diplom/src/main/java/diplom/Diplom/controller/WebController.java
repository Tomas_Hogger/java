package diplom.Diplom.controller;


import diplom.Diplom.controller.Helper.Helper;
import diplom.Diplom.controller.Helper.WeatherHelper;
import diplom.Diplom.entities.City;
import diplom.Diplom.entities.Country;
import diplom.Diplom.entities.Weather;
import diplom.Diplom.repository.Repository;
import org.jtwig.environment.EnvironmentConfiguration;
import org.jtwig.environment.EnvironmentConfigurationBuilder;
import org.jtwig.spring.JtwigViewResolver;
import org.jtwig.web.servlet.JtwigRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ViewResolver;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Controller
public class WebController {


    @Autowired
    private Repository repository;

    @GetMapping("/countries")
    @ResponseBody
    public String[] countries(){
        List<Country> list = repository.getAllCointries();
        return list.stream().map(Country::getName).toArray(String[]::new);
    }

    @GetMapping("/cities")
    @ResponseBody
    public String[] cities(@RequestParam(value = "country") String country){
        List<City> list = repository.getAllCities(country);
        return list.stream().map(City::getName).toArray(String[]::new);
    }

    @GetMapping("/weather")
    @ResponseBody
    public WeatherHelper[] weather(@RequestParam(value = "country") String country, @RequestParam(value = "city") String city){
        List<Weather> list = repository.getWeatherForCity(country, city);
        return list.stream().map(Helper::convert).toArray(WeatherHelper[]::new);
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("countries", repository.getAll());
        return "index";
    }

    @Bean
    public ViewResolver viewResolver(){
        EnvironmentConfiguration configuration = EnvironmentConfigurationBuilder
                .configuration()
                .parser()
                .syntax()
                .withStartCode("{%").withEndCode("%}")
                .withStartOutput("{=").withEndOutput("=}")
                .withStartComment("{#").withEndComment("#}")
                .and()
                .and()
                .resources()
                .withDefaultInputCharset(StandardCharsets.UTF_8)
                .and()
                .render()
                .withOutputCharset(StandardCharsets.UTF_8)
                .and()
                .build();

        JtwigRenderer jtwigRenderer = new JtwigRenderer(configuration);

        JtwigViewResolver viewResolver = new JtwigViewResolver();
        viewResolver.setRenderer(jtwigRenderer);
        viewResolver.setPrefix("classpath:/templates/");
        viewResolver.setSuffix(".twig.html");
        viewResolver.setContentType("text/html; charset=UTF-8");
        return viewResolver;
    }


}
