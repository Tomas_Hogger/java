package diplom.Diplom.repository;

import diplom.Diplom.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CountriesRepository extends JpaRepository<Country, Integer> {



    @Query("SELECT DISTINCT country FROM Country country JOIN FETCH country.cities")
    List<Country> findAllWithCities();




}
