package diplom.Diplom.repository;

import diplom.Diplom.entities.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CitiesRepository extends JpaRepository<City, Integer> {
    List<City> findAllByCountryName(String country);

    @Query("SELECT DISTINCT city FROM City city JOIN FETCH city.country country JOIN FETCH city.weathers weathers")
    List<City> findAllEager();

}
