package diplom.Diplom.repository;

import diplom.Diplom.entities.City;
import diplom.Diplom.entities.Country;
import diplom.Diplom.entities.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@org.springframework.stereotype.Repository
public class Repository {

    @Autowired
    CitiesRepository citiesRepository;

    @Autowired
    CountriesRepository countriesRepository;

    @Autowired
    WeatherRepository weatherRepository;

    @Transactional(readOnly = true)
    public List<Country> getAllCointries() {
        return countriesRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<City> getAllCities(String country) {
        return citiesRepository.findAllByCountryName(country);
    }

    @Transactional(readOnly = true)
    public List<Weather> getWeatherForCity(String country, String city) {
        return weatherRepository.findAllByCityCountryNameAndCityName(country,city);
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void save(List<Info> infoes){

        List<Weather> weathers = new ArrayList<>();
        HashMap<String,Country> cointries = new HashMap<>();
        HashMap<String, HashMap<String,City>> cities = new HashMap<>();
        for(Country country : countriesRepository.findAll()){
            cointries.put(country.getName(),country);
            HashMap<String,City> cities1 = new HashMap<>();
            for(City city : citiesRepository.findAll()){
                cities1.put(city.getName(),city);
            }
            cities.put(country.getName(),cities1);
        }

        for(Info info : infoes){
            String countryName = info.getCountryName();
            Country country = cointries.get(countryName);
            if(country == null){
                country = new Country();
                country.setName(countryName);
                countriesRepository.save(country);
                cointries.put(countryName,country);
                cities.put(countryName,new HashMap<>());
            }

            String cityName = info.getCityName();
            City city = cities.get(countryName).get(cityName);
            if(city == null){
                city = new City();
                city.setCountry(country);
                city.setName(cityName);
                citiesRepository.save(city);
                cities.get(countryName).put(cityName,city);
            }

            Weather weather = info.getWeather();
            Weather weatherAtDate = weatherRepository.findByCityAndTime(city,weather.getTime());
            if(weatherAtDate == null){
                weather.setCity(city);
                weathers.add(weather);
            }
        }

        weatherRepository.saveAll(weathers);
    }



    @Transactional(readOnly = true)
    public List<Country> getAll() {
        List<City> cities = citiesRepository.findAllEager();
        List<Country> countries = countriesRepository.findAllWithCities();
        return countries;
    }


}
