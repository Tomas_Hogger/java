package diplom.Diplom.repository;

import diplom.Diplom.entities.City;
import diplom.Diplom.entities.Weather;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface WeatherRepository extends JpaRepository<Weather, Long> {

    List<Weather> findAllByCityCountryNameAndCityName(String country, String city);
    Weather findByCityAndTime(City city, Date conditionDate);
}
