package diplom.Diplom.repository;


import diplom.Diplom.entities.Weather;

public class Info {

    private String countryName;
    private String cityName;
    private Weather weather;


    public Info(String countryName, String cityName, Weather weather){
        this.cityName = cityName;
        this.countryName = countryName;
        this.weather = weather;
    }


    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }
}
