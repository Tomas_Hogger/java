package diplom.Diplom.yahoo;

import diplom.Diplom.entities.Weather;
import diplom.Diplom.repository.Info;
import diplom.Diplom.repository.Repository;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service
@Configuration
public class YahooWeather {


    private static String path = "https://query.yahooapis.com/v1/public/yql?&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&q=";
    private static String formatQuery = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%s\")";
    private static String[] cities =
            {
                    "Moscow,Russia",
                    "St. Petersburg,Russia",
                    "Washington,USA",
                    "New York,USA",
                    "Seattle,USA",
                    "Berlin,Germany",
                    "Bonn,Germany",
                    "Paris,France",
                    "London,UK",
                    "Tokyo,Japan",
                    "Volgograd, Russia",
            };

    private static SimpleDateFormat dateFormat1 = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a z", Locale.US);
    private static SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm", Locale.US);


    @Autowired
    private Repository repository;

    @Scheduled(cron = "0 0/30 * * * ?")
    @Scheduled(initialDelay = 0, fixedDelay = Long.MAX_VALUE)
    public void update(){


        JSONParser jsonParser = new JSONParser();

        try {
            List<Info> infoes = new ArrayList<>();
            for (String city : cities) {
                String query = String.format(formatQuery, city);
                String fullPath = path + URLEncoder.encode(query, "UTF8");
                URL url = new URL(fullPath);


                JSONObject jsonObject;

                try (InputStreamReader inputStreamReader = new InputStreamReader(url.openStream())){
                    jsonObject = (JSONObject) jsonParser.parse(inputStreamReader);
                }

                JSONObject queryJson = (JSONObject) jsonObject.get("query");
                JSONObject result = (JSONObject) queryJson.get("results");
                JSONObject channel = (JSONObject) result.get("channel");
                JSONObject item = (JSONObject) channel.get("item");

                JSONObject location = (JSONObject) channel.get("location");
                JSONObject wind = (JSONObject) channel.get("wind");
                JSONObject atmosphere = (JSONObject) channel.get("atmosphere");
                JSONObject astronomy = (JSONObject) channel.get("astronomy");
                JSONObject condition = (JSONObject) item.get("condition");

                double windChill = doubleParser(wind.get("chill"));
                double windDirection = doubleParser(wind.get("direction"));
                double windSpeed = doubleParser(wind.get("speed"));
                double atmosphereHumidity = doubleParser(atmosphere.get("humidity"));
                double atmospherePressure = doubleParser(atmosphere.get("pressure"));
                double atmosphereVisibility = doubleParser(atmosphere.get("visibility"));
                Date astronomySunrise = dateParser(dateFormat2, astronomy.get("sunrise"));
                Date astronomySunset = dateParser(dateFormat2, astronomy.get("sunset"));
                Date conditionDate = dateParser(dateFormat1,condition.get("date"));
                double conditionTemp = doubleParser(condition.get("temp"));
                String conditionText = condition.get("text").toString();
                String cityName = location.get("city").toString();
                String countryName = location.get("country").toString();


                Weather weather = new Weather();
                weather.setSunrise(astronomySunrise);
                weather.setSunset(astronomySunset);
                weather.setHumidity(atmosphereHumidity);
                weather.setPressure(atmospherePressure);
                weather.setVisibility(atmosphereVisibility);
                weather.setWindChill(windChill);
                weather.setWindDirection(windDirection);
                weather.setWindSpeed(windSpeed);
                weather.setTime(conditionDate);
                weather.setTemp(conditionTemp);
                weather.setText(conditionText);

                infoes.add(new Info(countryName,cityName,weather));
            }

            repository.save(infoes);

        } catch (Exception e){

        }

    }

    private double doubleParser(Object object){
        return Double.valueOf(object.toString());
    }

    private Date dateParser(SimpleDateFormat simpleDateFormat, Object object) throws ParseException {
        return simpleDateFormat.parse(object.toString());
    }




}
