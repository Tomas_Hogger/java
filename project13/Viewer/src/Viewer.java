import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class Viewer extends JFrame {
    private JPanel rootPanel;
    private JButton chooseFileButton;
    private JTextArea textArea1;
    private JScrollPane scroll;
    private JFileChooser jFileChooser = new JFileChooser();
    private File file;

    {
        setSize(800,600);
        setLocationRelativeTo(null);
        setContentPane(rootPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }


    public Viewer(){
        chooseFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ret = jFileChooser.showDialog(null,"Выберите файл:");
                if(ret == JFileChooser.APPROVE_OPTION){
                    file = jFileChooser.getSelectedFile();
                }
            }
        });


    }


    public static void main(String[] args) {
        Viewer viewer = new Viewer();
    }
}
