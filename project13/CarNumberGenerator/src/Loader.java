import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Danya on 20.02.2016.
 */
public class Loader
{
    public static void main(String[] args) throws Exception
    {
        long start = System.currentTimeMillis();

        int ret = Runtime.getRuntime().availableProcessors();

        //ExecutorService executorService = Executors.newFixedThreadPool(ret);


        Thread[] threads = new Thread[ret];
        int numbers = 10000;
        int c = numbers/ret;
        int n = 1;

        for(int i = 0; i < ret; i++) {
            int finalN = n;
            int finalC = c;
            int finalI = i;
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {

                    try (PrintWriter printWriter = new PrintWriter(new StringBuilder("res/numbers").append(finalI).append(".txt").toString())) {
                        StringBuilder builder = new StringBuilder();
                        int bufferSize = 1_000_000;

                        char letters[] = {'У', 'К', 'Е', 'Н', 'Х', 'В', 'А', 'Р', 'О', 'С', 'М', 'Т'};
                        for (int number = finalN; number < finalN + finalC; number++) {
                            int regionCode = 199;
                            for (char firstLetter : letters) {
                                for (char secondLetter : letters) {
                                    for (char thirdLetter : letters) {
                                        if (builder.length() >= bufferSize) {
                                            printWriter.write(builder.toString());
                                            builder = new StringBuilder();
                                        }

                                        builder.append(firstLetter);
                                        if (number < 10) {
                                            builder.append("00");
                                        } else if (number < 100) {
                                            builder.append("0");
                                        }
                                        builder.append(number)
                                                .append(secondLetter)
                                                .append(thirdLetter);
                                        if (regionCode < 10) {
                                            builder.append("0");
                                        }
                                        builder.append(regionCode)
                                                .append('\n');
                                    }
                                }
                            }
                        }
                        printWriter.write(builder.toString());
                        printWriter.flush();
                        printWriter.close();

                        System.out.println((System.currentTimeMillis() - start) + " ms");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                }
            });
            n += c;
            int raz = numbers - n;
            c = raz > c && i+1 == ret?raz:c;
        }
        for(int i = 0; i < ret; i++){
            threads[i].start();
        }
    }
}
