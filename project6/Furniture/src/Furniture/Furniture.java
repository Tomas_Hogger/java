package Furniture;

abstract public class Furniture implements Comparable<Furniture> {
    private int weight;
    private int price;

    protected Furniture(int weight) {
        this.weight = weight;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public int getPrice(){
        return price;
    }

    public int getWeight(){
        return weight;
    }

    @Override
    public int compareTo(Furniture o) {
        if(this.weight == o.weight){
            return 0;
        } else if (this.weight > o.weight){
            return +1;
        } else {
            return -1;
        }
    }
}
