package Furniture;

import java.util.ArrayList;

public class Table extends Furniture {

    private ArrayList<String> numOfItems = new ArrayList<>();

    public Table(){
        super((int) (3000 + Math.random()*500));
    }

    public void put(String object){
        final int MAX_OF_OBJECTS = 5;
        if(numOfItems.size() == MAX_OF_OBJECTS){
            System.out.println("Стол полон");
        } else {
            numOfItems.add(object);
        }
    }


    public void removeAll(){
        if(numOfItems.size() != 0){
            numOfItems.clear();
        } else {
            System.out.println("На столе ничего нет");
        }
    }

}
