package Furniture;

public class Chair extends Furniture {

    private boolean free = true;

    public Chair(){
        super((int) (1000 + Math.random()*300));
    }

    public void sitDown(){
        if(free){
            free = false;
        } else {
            System.out.println("Занято");
        }
    }

    public void getUp(){
        if(free){
            System.out.println("Никто не сидит");
        } else {
            free = true;
        }
    }




}
