package Animals;

public class Sparrow extends FlyingBirds {

    public Sparrow(){
        setWeight((int) (25 + Math.random() * 10));
    }

    @Override
    public void voic() {
        System.out.println("Чик-чирик");
    }

    @Override
    public String getAnimal() {
        return "Sparrow";
    }
}
