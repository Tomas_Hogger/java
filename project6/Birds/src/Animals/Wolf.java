package Animals;

public class Wolf extends Beasts {

    public Wolf(){
        setWeight((int) (400 + Math.random() * 100));
    }

    @Override
    public void voic() {
        System.out.println("Ауууууу");
    }

    @Override
    public String getAnimal() {
        return "Wolf";
    }
}
