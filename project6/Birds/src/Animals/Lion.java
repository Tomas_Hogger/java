package Animals;

public class Lion extends Beasts {

    public Lion(){
        setWeight((int) (500 + Math.random() * 150));
    }

    @Override
    public void voic() {
        System.out.println("Ррррррр");
    }

    @Override
    public String getAnimal() {
        return "Lion";
    }
}
