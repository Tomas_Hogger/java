package Animals;

public class Ostrich extends NonFlyingBirds {

    public Ostrich(){
        setWeight((int) (200 + Math.random() * 100));
    }

    @Override
    public void voic() {
        System.out.println("ГРрррррр");
    }

    @Override
    public String getAnimal() {
        return "Ostrich";
    }
}
