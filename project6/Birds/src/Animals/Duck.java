package Animals;

public class Duck extends FlyingBirds {

    public Duck(){
       setWeight((int) (100 + Math.random() * 100));
    }

    @Override
    public void voic() {
        System.out.println("Кря-кря.");
    }

    @Override
    public String getAnimal() {
        return "Duck";
    }
}
