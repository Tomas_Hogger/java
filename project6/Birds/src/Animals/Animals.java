package Animals;

abstract public class Animals implements Comparable<Animals> {

    private int weight;

    abstract public void voic();

    public int getWeight(){
        return weight;
    }

    protected void setWeight(int weight){
        this.weight = weight;
    }

    abstract public String getAnimal();

    @Override
    public int compareTo(Animals o) {
        if(this.weight == o.weight){
            return 0;
        } else if (this.weight > o.weight){
            return +1;
        } else {
            return -1;
        }
    }
}
