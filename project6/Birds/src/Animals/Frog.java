package Animals;

public class Frog extends Beasts {

    public Frog(){
       setWeight((int) (50 + Math.random() * 10));
    }

    @Override
    public void voic() {
        System.out.println("Ква-ква.");
    }

    @Override
    public String getAnimal() {
        return "Frog";
    }
}
