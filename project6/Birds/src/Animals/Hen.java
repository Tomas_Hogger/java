package Animals;

public class Hen extends NonFlyingBirds {

    public Hen(){
         setWeight((int) (100 + Math.random() * 100));
    }

    @Override
    public void voic() {
        System.out.println("Ко-ко-ко.");
    }

    @Override
    public String getAnimal() {
        return "Hen";
    }
}
