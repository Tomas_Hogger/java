import Animals.*;

import java.util.ArrayList;
import java.util.Collections;

public class Loader {
    public static void main(String[] args) {
        ArrayList<Animals> animals = new ArrayList<>();
        addAnimals(animals);

        Collections.sort(animals);

        for(Animals animal:animals){
            System.out.print(animal.getAnimal() + " ");
            animal.voic();
        }


    }


    public static void addAnimals(ArrayList<Animals> animals){
        animals.add(new Duck());
        animals.add(new Frog());
        animals.add(new Hen());
        animals.add(new Lion());
        animals.add(new Ostrich());
        animals.add(new Sparrow());
        animals.add(new Wolf());
    }
}
