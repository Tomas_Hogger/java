import java.io.*;
import java.util.Scanner;

public class WriteFiles {
    public static void main(String[] args) throws IOException {
        System.out.print("Укажите путь файла: ");
        Scanner scanner = new Scanner(System.in);
        String path = scanner.nextLine();

        System.out.print("Укажите путь копирования: ");
        String pathCopy = scanner.nextLine();
        File file = new File(path);
        File fileCopy = new File(pathCopy);
        tree(file, fileCopy);
    }


    private static void tree(File file, File fileCopy) throws IOException {

        if(file.isDirectory()){
            if(fileCopy.isDirectory()) {
                if(!copyDirectory(file,fileCopy)){
                    System.err.println("<Ошибка>");
                }
            } else if(fileCopy.isFile()){
                System.err.println("<Копирование директории в файл невозможно!>");
            } else if(fileCopy.exists()){
                System.err.println("<Копирование не может быть совершенно>");
            } else if(fileCopy.mkdirs()){
                if(!copyDirectory(file,fileCopy)){
                    System.err.println("<Ошибка>");
                }
            } else {
                System.err.println("<Ошибка>");
            }
        } else if(file.isFile()){
            if(!fileCopy.exists() || fileCopy.isFile()) {
                copyFile(file,fileCopy);
            } else if(fileCopy.isDirectory()){
                System.err.println("<Копирование файла в директорию невозможно!>");
            } else {
                System.err.println("<Ошибка>");
            }
        } else if(file.exists()){
            System.err.println("<Копирование не может быть совершенно>");
        } else {
            System.err.println("<Файл не найден>");
        }
    }


    private static boolean copyDirectory(File file, File fileCopy) throws IOException {
        File[] files = file.listFiles();
        if(files != null){
            for(File innerFile : files){
                File destFile = new File(fileCopy.getAbsoluteFile(), innerFile.getName());
                tree(innerFile,destFile);
            }
            return true;
        } else {
            return false;
        }
    }

    private static void copyFile(File file, File fileCopy) throws IOException {

        try (InputStream inputStream = new FileInputStream(file);
             OutputStream outputStream = new FileOutputStream(fileCopy)) {
            byte[] buffer = new byte[1 << 20];
            for (int read = inputStream.read(buffer); read >= 0; read = inputStream.read(buffer)) {
                outputStream.write(buffer, 0, read);
            }
        }
    }
}
