import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;

public class FilesXMLandHTML {
    public static void main(String[] args) throws IOException{
        Scanner scanner = new Scanner(System.in);
        System.out.print("������� ����: ");
        String site = scanner.nextLine().trim();
        URI uri = null;
        try {
            uri = new URI(site);
        } catch (URISyntaxException e) {
            System.err.println("<������. ����������� ���� �����>");
            return;
        }

        System.out.print("������� ����, ���� ���������: ");
        String pathCopy = scanner.nextLine().trim();
        File file = new File(pathCopy);
        if(!file.isDirectory() && (file.exists() || !file.mkdirs())){
            System.err.println("<������. �� ������� ������� ����������>");
            return;
        }


        Document document = Jsoup.connect(uri.toString()).get();
        Elements elements = document.select("img");

        int i = 0;
        for(Element element : elements){
            i += 1;


            String path = element.absUrl("src");
            System.out.println(path);


            if(path.isEmpty() || path.charAt(path.length() - 1) == '/'){
                System.err.println("�������� �� ������� ������� �� ��������.");
                continue;
            }


            URL url = new URL(path);
            if(url.getQuery() != null) {
                System.err.println("�������� ������� ��������.");
                continue;
             }

            String nameImg = File.separator + path.substring(path.lastIndexOf('/') + 1);
            File image = new File(pathCopy, nameImg);


            try (InputStream inputStream = url.openStream();){
                Files.copy(inputStream, image.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }
}