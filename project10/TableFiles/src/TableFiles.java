import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class TableFiles {
    public static void main(String[] args) throws IOException {
        String pathIn = "res/Probabilites.txt";
        String pathOut = "res/Probabilities-formatted.txt";
        Charset charset = Charset.forName("CP1251");

        List<String> list = Files.readAllLines(Paths.get(pathIn), charset);


        String[][] table = new String[list.size()][];


        for(int i = 0; i < table.length; i ++){
            table[i] = list.get(i).split("\\s+");
        }

        int maxWeight = 0;

        for (String[] aTable1 : table) {
            for (String anATable1 : aTable1) {
                if (anATable1.length() > maxWeight) {
                    maxWeight = anATable1.length();
                }
            }
        }


        String format = "%-" + maxWeight + "s";



        try (PrintWriter printWriter = new PrintWriter(pathOut, charset.name())){
            for (String[] aTable : table) {
                for (int j = 0; j < aTable.length; j++) {
                    if (j > 0) {
                        printWriter.print("\t|\t");
                    }
                    printWriter.printf(format, aTable[j]);
                }
                printWriter.println();
            }
        } catch (Exception e) {
            System.err.println("<Ошибка>");
        }
    }
}
