import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


        public class FilesJsoun {
            public static void main(String[] args) throws IOException{
                HashMap<String,String> hashMap = new HashMap<>();
                Scanner scanner = new Scanner(System.in);

                while(true){
                    System.out.print("Имя,номер или спец.команда:");
                    String nameOrNumberOrCommand = scanner.nextLine();

                    if(nameOrNumberOrCommand.equals("LIST")){
                        if(!hashMap.isEmpty()){
                            ArrayList<String> arrayList = new ArrayList<>();
                            for(String num: hashMap.keySet()){
                                arrayList.add(hashMap.get(num)+ "=>" + num);
                            }
                            Collections.sort(arrayList);
                            for(String k : arrayList){
                                System.out.println(k);
                            }
                        } else {
                            System.out.println("Книга пуста.");
                        }
                    } else if(nameOrNumberOrCommand.equals("EXPORT")){
                        System.out.print("Укажите путь: ");
                        String path = scanner.nextLine();
                        File file = new File(path);
                        if(!file.isDirectory() && (file.exists() || !file.mkdirs())){
                            System.out.println("Копирование невозможно!");
                            continue;
                        }

                        File file1 = new File(path, "phoneBook.json");

                        JSONArray jsonArray = new JSONArray();

                        for(Map.Entry<String,String> entry : hashMap.entrySet()){
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("name", entry.getValue());
                            jsonObject.put("number", entry.getKey());
                            jsonArray.add(jsonObject);
                        }
                        try (PrintWriter printWriter = new PrintWriter(file1, Charset.forName("UTF-8").name())) {
                            jsonArray.writeJSONString(printWriter);
                        }

                    } else if(isName(nameOrNumberOrCommand)) {
                        boolean check = false;
                        for (String num : hashMap.keySet()) {
                            if (hashMap.get(num).equals(nameOrNumberOrCommand)) {
                                System.out.println(nameOrNumberOrCommand + "=>" + num);
                                check = true;
                            }
                        }
                        if(!check) {
                            System.out.print("Данного имени не найдено. Напиши номер:");
                            String number = scanner.nextLine();
                            if (isNumber(number)) {
                                if (hashMap.containsKey(number)) {
                                    System.out.print("Данный номер уже существует. Заменить имя? (YES/NO):");
                                    String answer = scanner.nextLine();
                                    if (answer.equals("YES")) {
                                        hashMap.replace(number, nameOrNumberOrCommand);
                                    }
                                } else {
                                    hashMap.put(number, nameOrNumberOrCommand);
                                }
                            } else {
                                System.out.println("Некоректный ввод.");
                            }
                        }
                    } else if (isNumber(nameOrNumberOrCommand)){
                        String name = hashMap.get(nameOrNumberOrCommand);
                        if(name != null){
                            System.out.println(name + "=>" + nameOrNumberOrCommand);
                        } else {
                            System.out.print("Данного номера не найдено. Напишите имя:");
                            name = scanner.nextLine();
                            if(isName(name)) {
                                hashMap.put(nameOrNumberOrCommand, name);
                            } else {
                                System.out.println("Некоректный ввод");
                            }
                        }
                    } else {
                        System.out.println("Некоректный ввод.");
                    }
                }
            }




            static boolean isName(String name){
                Pattern pattern = Pattern.compile("[A-Za-z]*[a-z]+");
                Matcher matcher = pattern.matcher(name);
                return matcher.matches();
            }

            static boolean isNumber(String number){
                Pattern pattern = Pattern.compile("[0-9]+");
                Matcher matcher = pattern.matcher(number);
                return matcher.matches();
            }
        }

