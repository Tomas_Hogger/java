import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class ReadingFiles {
    public static void main(String[] args) throws IOException {
        System.out.print("Укажите путь к файлу: ");
        Scanner scanner = new Scanner(System.in);
        String path = scanner.nextLine();
        File file = new File(path);
        tree(file, 0);
    }


    private static void tree(File file, int shift){





        if(file.isDirectory()) {
            print(file.getName(), shift);
            final int STEP = 1;
            shift += STEP;
            File[] files = file.listFiles();
            if (files != null) {
                for (File i : files) {
                    tree(i, shift);
                }
            } else {
                System.err.println("<Ошибка>");
            }
        } else if(file.isFile()){
            print(file.getName() + " " + file.length() + "b", shift);
        } else if(file.exists()){
            print(file.getName() + "<Unknown>", shift);
        } else {
            System.err.println("<Файл не найден>");
        }
    }

    private static void print(String name, int shift){
        for(int i = 0; i < shift; i++){
            System.out.print(" ");
        }
        System.out.println(name);
    }
}
