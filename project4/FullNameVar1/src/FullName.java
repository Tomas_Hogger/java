import java.util.Scanner;

public class FullName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String fullName = scanner.nextLine();
        fullName = fullName.trim();
        int firstIndex = fullName.indexOf(' ');
        int lastIndex = fullName.lastIndexOf(' ');
        System.out.println(
                "Фамилия: " + fullName.substring(0, firstIndex) + '\n' +
                "Имя: " + fullName.substring(firstIndex, lastIndex).trim() + '\n' +
                "Отчество: " + fullName.substring(lastIndex + 1)
        );
    }
}
