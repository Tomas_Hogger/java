import java.util.Scanner;

public class Number {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        number = number.replaceAll("[^0-9]+","");
        System.out.println(number);
    }
}
