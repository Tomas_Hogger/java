import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader
{
    public static void main(String[] args)
    {
        String text = "Вася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000 рублей";

        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(text);

        int sumSalary = 0;
        while (matcher.find()){
            sumSalary = sumSalary + Integer.parseInt(matcher.group());
        }

        System.out.println(sumSalary);
    }
}