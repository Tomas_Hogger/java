import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FullName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String fullName = scanner.nextLine();
        Pattern pattern = Pattern.compile(" *(?<Surname>[А-ЯЁ][а-яё]+) +(?<Name>[А-ЯЁ][а-яё]+) +(?<MiddleName>[А-ЯЁ][а-яё]+) *");
        Matcher matcher = pattern.matcher(fullName);

        if(matcher.matches()){
            System.out.println("Фамилия: " + matcher.group("Surname"));
            System.out.println("Имя: " + matcher.group("Name"));
            System.out.println("Отчество: " +matcher.group("MiddleName"));
        } else System.out.println("ФИО введено неправильно.");
    }
}
