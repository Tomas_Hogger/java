import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Birthday {
    public static void main(String[] args) {
        Date now = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2001, Calendar.NOVEMBER, 10);
        DateFormat birthday = new SimpleDateFormat("dd.MM.yyyy E");

        for(int i = 0;calendar.getTimeInMillis() <= now.getTime();calendar.add(Calendar.YEAR, 1),i++){
            System.out.println(i + "-" + birthday.format(calendar.getTime())) ;
        }
    }
}
