import java.util.Scanner;

public class Trucks {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Количество ящиков: ");
        int boxCount = in.nextInt();
        int boxNumber = 0;
        int truckNumber = 0;
        int containerNumber = 0;
        final int BOX_COUNT_IN_CONTAINER = 27;
        final int CONTAINER_COUNT_IN_TRUCKS = 12;


        for(; boxNumber < boxCount; truckNumber++){
            System.out.println("Грузовик " + (truckNumber+1));
            for (int maxContainerCountInThisTrucks = containerNumber + CONTAINER_COUNT_IN_TRUCKS;
                    containerNumber < maxContainerCountInThisTrucks && boxNumber < boxCount;containerNumber++){
                System.out.println("Контейнер " + (containerNumber+1));
                for(int maxBoxCountInThisContainer = boxNumber + BOX_COUNT_IN_CONTAINER;
                    boxNumber < maxBoxCountInThisContainer && boxNumber < boxCount; boxNumber++){
                    System.out.println("Ящик " + (boxNumber+1));
                }
            }
        }

        System.out.println();
        System.out.printf("Total:\n truck(s): %d \n container(s): %d \n box(es): %d",truckNumber,containerNumber,boxNumber);
    }
}
