
public class Cat
{
    private Double originWeight;
    private Double weight;
    private Double minWeight;
    private Double maxWeight;
    private static int count;
    private double eatenFood;


    //============


    public Cat()
    {
        this(1500.0 + 3000.0 * Math.random());
    }

    public Cat(Double weight){
        this(weight, weight, weight + 3000, weight < 1000 ? weight : 1000, 0);
    }

    private Cat(double weight, double originWeight, double maxWeight, double minWeight, double eatenFood)
    {
        this.weight = weight;
        this.originWeight = originWeight;
        this.minWeight = minWeight;
        this.maxWeight = maxWeight;
        this.eatenFood = eatenFood;
        if(isAlive()) {
            count = count + 1;
        }
    }

    public Cat(Cat cat)
    {
        this(cat.weight, cat.originWeight, cat.maxWeight, cat.minWeight, cat.eatenFood);
    }


    //===========


    public void meow() {
        if(isAlive()){
            addWeight(-1.0);
            System.out.println("Meow");
        }
    }

    public void feed(Double amount)
    {
        addEatenFood(amount);
        addWeight(amount);
    }

    public void drink(Double amount)
    {
        addEatenFood(amount);
        addWeight(amount);
    }

    public void goToilet() {
        if (isAlive()) {
            addWeight(-10.0);
            System.out.println("The cat went to the toilet");
        }
    }


    //===========

    public static int getCount(){ return count; }

    private static void setCount(int count){
        Cat.count = count;
    }


    public Double getWeight()
    {
        return weight;
    }

    private void setWeight(Double weight) {
        if(isAlive()){
            this.weight = weight;
            if(!isAlive()) {
                setCount(count - 1);
            }
        }
    }

    private void addWeight(Double amount) {
        if (isAlive()) {
            setWeight(weight + amount);
        }
    }


    public double getEatenFood() {
        return eatenFood;
    }

    private void setEatenFood(double eatenFood) {
        if (isAlive()) {
            this.eatenFood = eatenFood;
        }
    }

    private void addEatenFood(double eatenFood) {
        if (isAlive()){
            setEatenFood(this.eatenFood + eatenFood);
        }
    }


    public boolean isAlive(){
        return getWeight() >= minWeight && getWeight() <= maxWeight;
    }


    public String getStatus() {
        if(weight < minWeight) {
            return "Dead";
        }
        else if(weight > maxWeight) {
            return "Exploded";
        }
        else if(weight > originWeight) {
            return "Sleeping";
        }
        else {
            return "Playing";
        }
    }
}