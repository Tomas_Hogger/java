import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import threadpool.ThreadPool;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;


public class Sites extends JFrame {


    private JPanel rootPanel;
    private JTextField sitePath;
    private JButton startOrStopB;
    private JButton pauseB;
    private JTextField savePath;
    private File copyFile;
    private PrintWriter printWriter;
    private JButton chooseFileB;
    private JTextField numberPage;
    private JTextField timerText;
    private JFileChooser jFileChooser;

    private int count = Runtime.getRuntime().availableProcessors();
    private int delCount = 0;
    private URL url;
    private HashSet<String> hashSet = new HashSet<>();
    private ThreadPool threadPool;
    private int numberPageInt;
    private String startNumberPage = "Найдено 0 страниц.";
    private String startTimer = "00:00:00";
    private Timer timer;


    private final int START = 1, STOP = 2, PAUSE = 3;
    private int check;


    private long timeCor;
    private long time;

    {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setSize(800,600);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setContentPane(rootPanel);
        jFileChooser = new JFileChooser();
        check = STOP;
    }


    public Sites(){

        chooseFileB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ret = jFileChooser.showDialog(null, "Сохранить список в:");
                if (ret == JFileChooser.APPROVE_OPTION){
                    savePath.setText(jFileChooser.getSelectedFile().getPath());
                    copyFile = jFileChooser.getSelectedFile();
                }
            }
        });




        startOrStopB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (check == STOP) {
                    String locSitePath = sitePath.getText();
                    String locSavePath = savePath.getText();

                    if (locSitePath == null || locSitePath.equals("")) {
                        JOptionPane.showMessageDialog(rootPanel, "Укажите URL сайта!", "Error!", JOptionPane.ERROR_MESSAGE);
                    } else if (locSavePath == null || locSavePath.equals("") || !copyFile.isFile()) {
                        JOptionPane.showMessageDialog(rootPanel, "Укажите файл сохранения!", "Error!", JOptionPane.ERROR_MESSAGE);
                    } else {
                        numberPageInt = 0;
                        numberPage.setText(startNumberPage);
                        timerText.setText(startTimer);

                        try {
                            printWriter = new PrintWriter(copyFile);
                        } catch (FileNotFoundException e1) {
                            e1.printStackTrace();
                        }


                        //executorService = Executors.newFixedThreadPool(count);

                        threadPool = new ThreadPool(count);

                        try {
                            url = new URL(locSitePath);
                        } catch (MalformedURLException e1) {
                            e1.printStackTrace();
                        }


                        timeCor = System.currentTimeMillis();
                        time = 0;
                        timer = new Timer(100, new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if(check == PAUSE){
                                    timeCor = System.currentTimeMillis() - time;
                                }
                                time = System.currentTimeMillis() - timeCor;
                                timerText.setText(String.format("%02d:%02d:%02d", time / 1000 / 3600, time / 1000 / 60 % 60, time / 1000 % 60));
                                numberPage.setText(String.format("Найдено %d страниц.", numberPageInt));
                                if(delCount == 0){
                                    try {
                                        stop();
                                    } catch (Exception e1) {
                                        //e1.printStackTrace();
                                    }
                                }
                            }
                        });


                        try {
                           Document document = Jsoup.connect(locSitePath).ignoreContentType(true).get();
                            try {
                                 threadPool.addTask(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {

                                            addCount(1);
                                            write(document, 0);
                                            addCount(-1);
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                });
                            } catch (Exception e1) {
                                JOptionPane.showMessageDialog(rootPanel, "Ошибка!", "Error!", JOptionPane.ERROR_MESSAGE);
                            }
                        } catch (IOException e1) {
                            JOptionPane.showMessageDialog(rootPanel, "Некорректный URL!", "Error!", JOptionPane.ERROR_MESSAGE);
                        }

                        timer.start();

                        startOrStopB.setText("Stop");
                        check = START;
                    }
                } else {

                    try {
                        stop();
                    } catch (Exception e1) {
                        //e1.printStackTrace();
                    }
                }
            }

        });


        pauseB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(check == START){
                    check = PAUSE;
                    threadPool.setPaused(true);
                    pauseB.setText("Start");
                } else if(check == PAUSE){
                    check = START;
                    threadPool.setPaused(false);
                    pauseB.setText("Pause");
                }
            }
        });

    }


    private synchronized void stop() throws Exception {
        if(check != STOP) {
            if (check == PAUSE)
                pauseB.setText("Pause");
            startOrStopB.setText("Start");
            check = STOP;
            threadPool.close();
            printWriter.close();
            hashSet = new HashSet<>();
            timer.stop();
        }
    }



    private void write(Document document, int i) throws Exception {
        Elements elements = document.select("a");
        for (Element element : elements) {
            if (isEmptyPul(element, i)) continue;
            wrtieDop(element, i);
        }
    }

    private synchronized boolean isEmptyPul(Element element, int i) throws Exception {
            if (delCount < count) {
                threadPool.addTask(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            addCount(1);
                            wrtieDop(element, i + 1);
                            addCount(-1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                return true;
            } else {
                return false;
            }
    }

    private void wrtieDop(Element element, int i) throws Exception {
        if(threadPool.isPaused()) pause();
        if(!threadPool.isDisposed()) {
            String path = element.absUrl("href");
            if (!path.equals("")) {
                URL url1 = new URL(path);
                addNumberPage();
                if (url1.getHost().equals(url.getHost())
                        && url1.getProtocol().equals(url.getProtocol())
                        && url1.getPort() == url.getPort()
                        && add(path)
                        ) {
                    writeToFile(i,path);
                    Document document1 = null;
                    try {
                        document1 = Jsoup.connect(path).get();
                    } catch (HttpStatusException e) {
                        return;
                    }
                    write(document1, i + 1);
                }
            }
        }
    }


    private void pause(){
        while(threadPool.isPaused()){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }
    }


    private synchronized void addCount(int i){
        delCount += i;
    }

    private synchronized void writeToFile(int i, String str){
        for (int j = 0; j < i; j++) {
            printWriter.write(" ");
        }
        printWriter.write(str + "\r\n");
    }

    private synchronized boolean add(String str) {
        return hashSet.add(str);
    }

    private synchronized void addNumberPage(){
        numberPageInt++;
    }




    public static void main(String[] args) {
        int count = Runtime.getRuntime().availableProcessors();

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Sites sites = new Sites();
                sites.setVisible(true);
            }
        });
    }

}
