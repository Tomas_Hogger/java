import javax.swing.*;

public class Loader {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MyForm myForm = new MyForm();
                myForm.setVisible(true);
            }
        });
    }
}
