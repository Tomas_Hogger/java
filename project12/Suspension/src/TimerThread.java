import javax.swing.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class TimerThread extends SwingWorker<Long, Long> {

    private AtomicBoolean atomicBoolean = new AtomicBoolean(false);

    @Override
    protected Long doInBackground() throws Exception {

        long timeCor = System.currentTimeMillis();
        long time = 0;
        while (!Thread.interrupted()){
            if(atomicBoolean.get()){
                timeCor = System.currentTimeMillis() - time;
            }
            time = System.currentTimeMillis() - timeCor;
            if(!atomicBoolean.get()) {
                publish(time);
            }
            Thread.sleep(100);
        }


        return null;
    }


    @Override
    protected void process(List<Long> chunks) {
        super.process(chunks);
        long value = chunks.stream().max(Long::compare).get();
        process(value);
    }


    abstract public void process(long time);

    public boolean isPaused() {
        return atomicBoolean.get();
    }

    public void setPaused(boolean paused){
        atomicBoolean.set(paused);
    }

}
