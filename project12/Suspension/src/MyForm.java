import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class MyForm extends JFrame {
    private JPanel rootPanel;
    private JTextField jTextField;
    private JButton buttonStart;
    private JButton buttonPause;
    TimerThread timerThread;

    public MyForm(){


        {
            jTextField.setHorizontalAlignment(SwingConstants.RIGHT);
            setSize(800,600);
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            setLocationRelativeTo(null);
            setContentPane(rootPanel);
        }

        buttonStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(timerThread == null) {
                    timerThread = new TimerThread() {
                        @Override
                        public void process(long time) {
                            if(timerThread != null) {
                                setTime(time);
                            }
                        }
                    };
                    timerThread.execute();
                    buttonStart.setText("Stop");
                } else{
                    if(timerThread.isPaused()){
                        buttonPause.setText("Pause");
                    }
                    setTime(0);
                    buttonStart.setText("Start");
                    timerThread.cancel(true);
                    timerThread = null;
                }
            }
        });

        buttonPause.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (timerThread != null) {
                    if (!timerThread.isPaused()) {
                        timerThread.setPaused(true);
                        buttonPause.setText("Start");
                    } else {
                        timerThread.setPaused(false);
                        buttonPause.setText("Pause");
                    }
                }
            }
        });
    }

    private void setTime(long time){
        jTextField.setText(String.format("%02d:%02d:%02d", time / 1000 / 3600, time / 1000 / 60 % 60, time / 1000 % 60));
    }



}
