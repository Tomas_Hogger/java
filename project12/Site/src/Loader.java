import javax.swing.*;

import Site.Site;

public class Loader {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Site site = new Site();
                site.setVisible(true);
            }
        });


    }

}
