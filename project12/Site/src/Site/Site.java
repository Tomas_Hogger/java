package Site;

import downloader.Data;
import downloader.State;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;


public class Site extends JFrame {


    private JPanel rootPanel;
    private JButton chooseFileB;
    private JButton startOrStopB;
    private JButton pauseB;
    private JButton saveB;
    private JTextField savePath;
    private JTextField numberPage;
    private JTextField sitePath;
    private JTextField timerText;
    private JFileChooser jFileChooser;

    private Data data;
    private Timer timer;
    private Map<String, Set<String>> urlMap;


    {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setSize(800,600);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setContentPane(rootPanel);
        jFileChooser = new JFileChooser();
        saveB.setEnabled(false);
    }




    public Site() {

        chooseFileB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ret = jFileChooser.showDialog(null, "Сохранить список в:");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    savePath.setText(jFileChooser.getSelectedFile().getPath());
                }
            }
        });


        startOrStopB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (data == null) {
                    setTextNumberPage(0);
                    setTextTime(0);
                    saveB.setEnabled(false);
                    String url = sitePath.getText();
                    if (url != null && Pattern.matches("https?://[^ ]+", url.trim())) {
                        data = new Data(Runtime.getRuntime().availableProcessors() * 2, url.trim());
                        timer = new Timer(100, new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                State state = data.getState();
                                setTextTime(state.getTimeElapsed() / 1000);
                                setTextNumberPage(state.getLinksDetectedCount());
                                if (data.isDone()) {
                                    stop();
                                }
                            }
                        });
                        timer.start();
                        startOrStopB.setText("Stop");
                    } else {
                        JOptionPane.showMessageDialog(rootPanel, "Укажите URL сайта!", "Error!", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    data.setPaused(true);
                    if (JOptionPane.showConfirmDialog(rootPanel, "Вы уверены, что хотите прервать выполнение?", "", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        stop();
                    } else {
                        data.setPaused(false);
                    }
                }
            }
        });


        pauseB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (data != null) {
                    if (data.isPaused()) {
                        data.setPaused(false);
                        pauseB.setText("Pause");
                    } else {
                        data.setPaused(true);
                        pauseB.setText("Continue");
                    }
                }
            }
        });


        saveB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Map<String,Set<String>> map;
                if (urlMap != null) {
                    map = urlMap;
                try (PrintStream printStream = new PrintStream(jFileChooser.getSelectedFile(),StandardCharsets.UTF_8.name())){

                        startOrStopB.setEnabled(false);
                        saveB.setEnabled(false);
                        printUrl(map,new HashSet<>(),  printStream);
                        JOptionPane.showMessageDialog(rootPanel, "Сохранение прошло успешно!", "!", JOptionPane.INFORMATION_MESSAGE);

                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(rootPanel,"Ошибка!", "!", JOptionPane.ERROR_MESSAGE);
                } finally {
                    startOrStopB.setEnabled(true);
                    saveB.setEnabled(true);
                    }
                } else {
                JOptionPane.showMessageDialog(rootPanel, "Не найдено ссылок!", "Error!", JOptionPane.ERROR_MESSAGE);
            }
            }
        });
    }

    private void  printUrl(Map<String,Set<String>> map, Set<String> alreadyPrint, PrintStream printStream){
        for(String str : map.keySet()){
            print(str, map,alreadyPrint,0,printStream);
        }
        printStream.close();
    }

    private void print(String str,Map<String,Set<String>> map, Set<String> alreadyPrint, int level, PrintStream printStream){
        if(alreadyPrint.add(str)){
            for(int i = 0; i < level; i++){
                printStream.print(" ");
            }
            printStream.print(str + "\r\n");
            Set<String> sets = map.get(str);
            if(sets != null) {
                for (String set : sets) {
                    print(set,map,alreadyPrint,level+1,printStream);
                }
            }
        }
    }

    private synchronized void stop(){
        if(data != null) {
            if(data.isPaused()){
                pauseB.setText("Pause");
            }
            data.close();
            timer.stop();
            startOrStopB.setText("Start");
            urlMap = data.getUrlMap();
            saveB.setEnabled(true);
            data = null;
        }
    }

    private void setTextNumberPage(int num){
        setText(numberPage,"Просмотрено " + num + " страниц(а)");
    }

    private void setTextTime(long time){
        setText(timerText, "Прошло " + time + " секунд(ы).");
    }

    private void setText(JTextField jTextField, String str){
        jTextField.setText(str);
    }






}
