package Bank;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by Danya on 18.02.2016.
 */
public class Bank {
    private final Random random = new Random();
    private final int CONTAIN = 0, ONLY_EQUALS = 1, NO_EQUALS = 2;
    private HashMap<String, Account> accounts;

    {
        accounts = new HashMap<>();
    }

    private synchronized boolean isFraud(String fromAccountNum, String toAccountNum, long amount)
            throws InterruptedException {
        Thread.sleep(1000);
        return random.nextBoolean();
    }

    /**
     * TODO: ����������� �����. ����� ��������� ������ ����� �������.
     * ���� ����� ���������� > 50000, �� ����� ���������� ����������,
     * ��� ������������ �� �������� ������ ������������ � ����������
     * ����� isFraud. ���� ������������ true, �� �������� ����������
     * ������ (��� � �� ���� ����������)
     */
    public boolean transfer(String fromAccountNum, String toAccountNum, long amount) throws InterruptedException {
        int check1 = isAccount(fromAccountNum);
        int check2 = isAccount(toAccountNum);

        if (check1 == CONTAIN && check2 == CONTAIN) {
            Account account1 = accounts.get(fromAccountNum);
            if (account1.getMoney() < amount) {
                return false;
            }
            Account account2 = accounts.get(toAccountNum);
            if (!account1.takeAwayMoney(amount) || !account2.addMoney(amount)) {
                return false;
            }

            if(amount > 50_000){
                account1.setTemporaryBlock(true);
                account2.setTemporaryBlock(true);
                if(isFraud(fromAccountNum,toAccountNum,amount)){
                    account1.setEternalBlock(true);
                    account2.setEternalBlock(true);
                } else {
                    account1.setTemporaryBlock(false);
                    account2.setTemporaryBlock(false);
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * TODO: ����������� �����. ���������� ������� �� �����.
     */
    public long getBalance(String accountNum) {
        int check = isAccount(accountNum);
        if (check == CONTAIN) {
            return accounts.get(accountNum).getMoney();
        } else {
            return 0;
        }
    }

    public synchronized boolean createAccount(String accountName) {
        int check = isAccount(accountName);
        if (check == ONLY_EQUALS) {
            accounts.put(accountName, new Account(accountName, randomMoney()));
            return true;
        } else {
            return false;
        }
    }


    private int isAccount(String accountName) {
        if (accountName != null) {
            accountName.trim();
            if (!accountName.equals("")) {
                if (accounts.containsKey(accountName)) {
                    return CONTAIN;
                }
                return ONLY_EQUALS;
            } else {
                return NO_EQUALS;
            }
        } else {
            return NO_EQUALS;
        }
    }



    private long randomMoney() {
        return (long) (100_000 * random.nextDouble());
    }
}
