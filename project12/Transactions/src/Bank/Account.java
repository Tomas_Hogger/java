package Bank;

/**
 * Created by Danya on 18.02.2016.
 */
public class Account
{

    private long money;
    private String accNumber;
    private boolean temporaryBlock;
    private boolean eternalBlock;

    protected Account(String accNumber, long money){
        this.accNumber = accNumber;
        this.money = money;
        this.temporaryBlock = false;
        this.eternalBlock = false;
    }

    protected void setTemporaryBlock(boolean ch){
        temporaryBlock = ch;
    }

    protected void setEternalBlock(boolean ch){
        eternalBlock = ch;
    }

    public long getMoney() {
        return money;
    }

    private void setMoney(long money) {
        this.money = money;
    }

    protected synchronized boolean takeAwayMoney(long money){
        if(!isBlock()) {
            setMoney(getMoney() - money);
            return true;
        }
        return false;
    }

    protected synchronized boolean addMoney(long money){
        if(!isBlock()) {
            setMoney(getMoney() + money);
            return true;
        }
        return false;
    }

    private boolean isBlock(){
        while (!eternalBlock && temporaryBlock){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return eternalBlock;
    }

    public String getAccNumber() {
        return accNumber;
    }

    protected void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }
}
