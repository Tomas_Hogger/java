import Bank.Bank;
import org.junit.Test;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.TreeMap;

public class Loader {

    private static Random random = new Random();
    private static Bank bank = new Bank();

    private final static int TRANSFER = 0;
    private final static long MONEY_1 = 1_000, MONEY_2 = 10_000, MONEY_3 = 50_000, MONEY_4 = 51_000;
    private final static double VAR_MONEY_1 = 0.3, VAR_MONEY_2 = 0.6, VAR_MONEY_3 = 0.9;
    private final static int NUMBER_OF_CLIENT = 1_000;


    @Test
    public void test() throws InterruptedException {

        Thread[] threads = new Thread[NUMBER_OF_CLIENT];
        for(Integer i = 0; i < NUMBER_OF_CLIENT /2; i++){
            String account = String.valueOf(i);
            bank.createAccount(account);
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    client(account);
                }
            });

        }

        for (int i = 0; i < NUMBER_OF_CLIENT /2; i++){
            threads[i].start();
        }


        for(int i = NUMBER_OF_CLIENT/2; i < NUMBER_OF_CLIENT; i++){
            String account = String.valueOf(i);
            bank.createAccount(account);
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    client(account);
                }
            });
            Thread.sleep(1000);
        }
    }


    private void client(String account){
        while (true){
            if(random.nextInt(2) == TRANSFER){
                String account2 = String.valueOf(random.nextInt(NUMBER_OF_CLIENT));
                double var = random.nextDouble();
                long money;
                if(var < VAR_MONEY_1){
                    money = MONEY_1;
                } else if( var < VAR_MONEY_2){
                    money = MONEY_2;
                } else if (var < VAR_MONEY_3){
                    money = MONEY_3;
                } else {
                    money = MONEY_4;
                }
                try {
                    if(bank.transfer(account, account2, money)){
                        System.out.printf("Money (%d) transferred from account %s to account %s   (%d, %d)", money, account, account2, bank.getBalance(account), bank.getBalance(account2));
                        System.out.println();
                    } else {
                        System.out.printf("Transfer from account %s is denied (%d, %d , %s)", account,bank.getBalance(account), money, account2);
                        System.out.println();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.printf("Account balance %s is %d", account,bank.getBalance(account));
                System.out.println();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }



}